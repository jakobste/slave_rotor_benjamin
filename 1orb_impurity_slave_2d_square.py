import numpy as np
from numpy import linalg as LA
import os.path
import sys
import time
################# TRIQS LIBRARIES ################
from pytriqs.gf import *
from pytriqs.archive import HDFArchive
from triqs_cthyb import Solver
import pytriqs.operators.util as op
import pytriqs.utility.mpi as mpi
from pytriqs.atom_diag import trace_rho_op
################# OTHER SUBROUTINES ##############
sys.path.append("/home/jakobste/DMFT/TRIQS/TEST_3DSQUARE_LATTICE/slave_rotor_benjamin/subroutines/") # REPLACE WITH NEW PATH!!!
from Gloc_parallel import G0_square_lattice, calc_G0_parallel, calc_G0, calc_dN_dE
from Rotate_Basis import transf_Delta_phi, transf_Delta_auto
from Rotor_Greensfun import Rotor_Greens
from G0_nonparallel_SR_UdpisUpp import calculate_G0_SR_UdpUpp


comm    = mpi.MPI.COMM_WORLD
mpisize = comm.size
mpirank = comm.rank

mixen_fac = 0.4
#parms = np.array([50, 4., 4., 0.6, -0.38 , 0., 0., 1.2, 1.2, 0., 0., 10, 0, 0])
parms=np.loadtxt('dmft.input')

mpi.report('parameters = ', parms)
# General parameters
Nomeg = 2048
Ntau = Nomeg*2+1
l = 1                                        # Angular momentum
n_orbs = 2                                   # Number of orbitals

mpi.report(sys.argv)
if ('notconst' in sys.argv):
   mpi.report('Chose notconst slave rotor!')
if ('flipGX' in sys.argv):
    mpi.report('Flip GX in tau!')

######################### READ IN PARAMETERS #############################
beta = parms[0]                              # Inverse temperature
############## Coulomb interactions ###########
Ud = parms[1]                                 # Screened Coulomb interaction
Up = parms[2]
Udp= parms[3]
assert Up == Udp, "Up not equal to Udp -> crucial for this implementation!!!"

mu = parms[4]
# Atomic energy levels and hybridization
Ed = parms[5]
Ep = parms[6]
Vdp = parms[7]
Vpd = parms[8]

atom_en = np.zeros((2,2)) 
atom_en[0,0] = Ed
atom_en[1,1] = Ep
atom_en[0,1] = Vdp
atom_en[1,0] = Vpd

############## hopping parameters #############
tdd = parms[9] # 1/8 of dd bandwidth for model on 2D square lattice 
tpp = parms[10]
tdp = parms[11]
tpd = parms[12]
t_hop = np.array([[tdd, tdp], [tpd, tpp]])

half_bandwidth = 2.0                         # Half bandwidth

METHOD_NAME = "none"
if parms[14] == 1:
    METHOD_NAME = "Slave_Rotor"
    mpi.report('Use SlaveRotor Greens function!')
elif parms[14] == 2:
    METHOD_NAME = "Slave_Spin"
    mpi.report('Use SlaveSpin Greens function!')
elif parms[14] == 0:
    mpi.report('Use shell folding only!')
elif parms[14] == 3:
    METHOD_NAME = "Slave_Rotor_const"
    mpi.report('Use SlaveRotor Greens function w. const factor!')
elif parms[14] == 4:
    METHOD_NAME = "Slave_Spin_const"
    mpi.report('Use SlaveSpin Greens function w. const factor!')  
elif parms[14] == 5:
    METHOD_NAME = "Slave_Rotor_fully_const"
    mpi.report('Use Slave Rotor renormalizations only!') 
else:
    METHOD_NAME = "Sigma_Model"
    mpi.report('Use Sigma Model representation to solve the problem!')  


xmu = mu

mpi.report('xmu = ' + str(xmu) + 'Ed eff = ' +str(atom_en[0,0]) + ' Ep eff = ' + str(atom_en[1,1]) + ' Edp eff = ' + str(atom_en[0,1]) + ' Epd eff = ' + str(atom_en[1,0]))
mpi.report('ATTENTION WITH CURRENT IMPLEMENTATION IF MU IS FIXED!!!')

if int(parms[13]) == 0:
    fixmu = True 
    Nfill_end = 0
else:
    fixmu = False
    Nfill_end = parms[13]                                  # filling to be calculated
mpi.report('fixmu = ', fixmu, 'Nfill = ', Nfill_end) 

# In case we want to fix Nd and Np !!!
if int(parms[15]) == 0:
    fix_charges = False
else:
    fix_charges = True
    Nd_end = parms[15]
    Np_end = Nfill_end - Nd_end

dEd_old = 0.
Nd_old = 0. 
firstNdupdate = True
Elevel_update = False


mu_maxstep = 0.5
    
off_diag = False                              # Include orbital off-diagonal elements?
filename = 'test.h5' # 'slater_two_mpitest_Ud%s_Udp%s_mu%s.h5' %(Ud,Udp,mu) # Name of file to save data to
densfile = 'test_dens.txt' #'slater_two_densities_Ud%s_Udp%s_mu%s.txt' %(Ud,Udp,mu) # Name of file to save density data to

oldsim=False
Noldit=0
extinp = False


# Solver parameters
p = {}
n_cycles = [400000, 40000]
p["n_warmup_cycles"] = n_cycles[1]     # Number of warmup cycles to equilibrate
p["n_cycles"] = n_cycles[0]          # Number of measurements
p["length_cycle"] = 200          # Number of MC steps between consecutive measures
p["move_double"] = True          # Use four-operator moves
#p["random_name"] = 'mt19937'     # Name of random number generator
p["measure_density_matrix"] = True  # Measure the reduced density matrix
p["use_norm_as_weight"] = True      # Required to measure the density matrix

        
# Block structure of Green's functions
gf_struct_solver = [ ['up',[0]], ['down',[0]] ] # op.set_operator_structure(spin_names,Solver_orb,off_diag=off_diag)

    
# Construct the solver
S = Solver(beta=beta, gf_struct=gf_struct_solver, n_iw = Nomeg)

# Construct interaction Hamiltonian
Hint = (Ud - Udp) * op.n('up', 0) * op.n('down', 0)

# First Sigma should be zero
Sig0_iw = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)
Sig0_iw << 0

from interpolate_G_new_beta import change_temperature


# If old data is available, use it
if len(sys.argv) > 4:
    mpi.report('Use Sig0_iw from old simulation: ' + sys.argv[3] + ' , ' + sys.argv[4] + 'th iteration')
    betaold = HDFArchive(sys.argv[3])['Sigsym_iw-%s'%(int(sys.argv[4]))].mesh.beta
    mpi.report('Old simulation performed at beta = ' + str(betaold))
    if betaold == beta:
        Sig0_iw << HDFArchive(sys.argv[3])['Sigsym_iw-%s'%(int(sys.argv[4]))]
    else:
        mpi.report('Change temperature from beta(old) = ' + str(betaold) + ' to beta(new) = ' + str(beta))
        Sig0_iw << change_temperature(beta, Nomeg, HDFArchive(sys.argv[3])['Sigsym_iw-%s'%(int(sys.argv[4]))])
else:
    mpi.report('No old simulation data found')

# Calculate G0 for solver, using Slave Rotor representation and basis rotation
cos_exp_old = 1.
Ekin = None # Kinetic energy not yet defined
GX_tau_old = None # also not yet defined
Nfill_imp = 0.
G0_sol = None # not defined yet
ALLOW_SR_UPDATE = False #boolian defining whether to allow SR update
### provide Sig0_iw = 0 for Sigma, but also for Gtilde_iw (since it's not used anyhow if Ekin == None) !
G0_params = beta, Nomeg, Ntau, mu, Nfill_imp, Nfill_end, Udp, atom_en, t_hop, Sig0_iw, Sig0_iw, filename, cos_exp_old, Ekin, GX_tau_old, G0_sol, True
Gd_solver, G0_solver, Delta_SR, Delta_init, G0_initial, GX_tau, cos_theta_exp, Ekin, SR_UPDATE = calculate_G0_SR_UdpUpp(G0_params, METHOD_NAME, mpirank)
# Update GX_tau_old and use it to make convergence more stable
GX_tau_old = GX_tau
    
if mpirank == 0:
    with HDFArchive(filename,'a') as Results:
        Results['Ginit_iw'] = G0_solver
        Results['Gd_init_iw'] = Gd_solver
        Results['G0_init_iw'] = G0_initial
        Results['Delta_iw'] = Delta_SR
        #Results['Delta_iw_in'] = Delta_init
    
Gd_solver = Gd_solver.make_real_in_tau()

for name, g0 in S.G0_iw:
    g0 << Gd_solver

mpi.report('Assigned G0 to the solver!')


        
#######################################################################################################
#############################       Now start the DMFT loops      #####################################
#######################################################################################################

cos_theta_list = [cos_theta_exp] # List to collect cos_thetas
Ekin_latt_list = [Ekin] # List to collect lattice energies
xmu = mu 
xmu_old   = None
Nfill_old = None
new_solution = None
dNdEp = 0.
maxerr = 1.
i_loop = 0
Nd = 1.
Np = 1.
relative_error = 0.02
n_mu_ind = 0
n_mu_vec = np.zeros((1000,2))
weightvec = np.zeros(1000)

#for i_loop in range(1):
while ((maxerr > 1e-2) or (i_loop < 50) ):
    mpi.report('nth iteration: ',i_loop)
    # Determine the new Weiss field G0_iw self-consistently
    
    ########### if fixmu = False, calculate update for xmu #############
    if (i_loop > 0):
        if (i_loop > 1) & (fixmu == False):
            if i_loop == 2:
                xmu_old   = xmu 
                Nfill_old = Nfill_imp
                # Estimate change of density with mu 
                dmu = 0.01
                G1 = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)
                G1 << inverse(inverse(G0_initial) + dmu - Sig_sym_iw)

                fixed_coeff = np.array([[[0.,0.],[0., 0.]], [[1.,0.],[0., 1.]]], dtype=complex)
                t1, e1 = G1.fit_tail_on_window(int(Nomeg*0.3), int(Nomeg*0.7), fixed_coeff, 3,3)
                G1.replace_by_tail(t1, int(Nomeg*0.7))

                dndmu = 2.*np.trace(G1.density()).real - Nfill_imp
                dndmu = dndmu/dmu + 1e-14
                delta_xmu = (Nfill_end - Nfill_imp)/dndmu
                if np.abs(delta_xmu) > mu_maxstep:
                    delta_xmu = + np.sign(Nfill_end - Nfill_imp)*mu_maxstep
                xmu = xmu + delta_xmu

                #mpi.report('newmu ', xmu)
            if fix_charges and (np.abs(Nfill_imp - Nfill_end) < Nfill_end*relative_error/2.) and (i_loop > 3) :
                mpi.report('FIX CHARGES - > NO MU UPDATE NOW!')
                
            elif ((i_loop%2 == 0 ) or (i_loop > 10)) & (i_loop > 2) & (SR_UPDATE or Elevel_update):
                delta_xmu = (Nfill_end - Nfill_imp)/dndmu
                if np.abs(delta_xmu) > mu_maxstep:
                    delta_xmu = + np.sign(Nfill_end - Nfill_imp)*mu_maxstep
                xmu_old   = xmu 
                Nfill_old = Nfill_imp
                xmu = xmu + delta_xmu
                SR_UPDATE = False
            else:
                if np.abs(Nfill_imp - Nfill_end) > 0.05: #n_mu_ind < 4:
                    dndmu = (Nfill_imp - Nfill_old)/(xmu - xmu_old)
                    delta_xmu = (Nfill_end - Nfill_imp)/dndmu
                    if np.abs(delta_xmu) > mu_maxstep:
                        delta_xmu = + np.sign(Nfill_end - Nfill_imp)*mu_maxstep
                    xmu_old   = xmu 
                    Nfill_old = Nfill_imp
                    xmu = xmu + delta_xmu
                else:
                    sigma_iw_dat = np.squeeze(Sig_sym_iw.data)
                    dndmu_old = (Nfill_imp - Nfill_old)/(xmu - xmu_old)
                    mpi.report('Calculate dn/dmu now')
                    G0_2dlatt = G0_square_lattice(100, atom_en, t_hop)
                    dN_dEnew, dNdE_mat = calc_dN_dE(beta, Nomeg, sigma_iw_dat, xmu, G0_2dlatt, EFFICIENT = True)
                    del G0_2dlatt
                    dndmu = -2.*np.sum(dNdE_mat).real # Factor 2 to aid convergence. Attention, gives complex output!!!  
                    mpi.report('dndmu calculated = ' + str(dndmu) + ' dndmu measured = ' + str(dndmu_old))
                    delta_xmu = (Nfill_end - Nfill_imp)/dndmu
                    xmu_old   = xmu
                    Nfill_old = Nfill_imp
                    xmu = xmu + delta_xmu

                
        if fix_charges and (np.abs(Nfill_imp - Nfill_end) < 0.01) and (not (Elevel_update or SR_UPDATE)):
            dNd = Nd_end-Nd
            mpi.report('Update Ep !!!, Nd_end - Nd = '+str(dNd))
            if firstNdupdate:
                dEpold = - np.sign(dNd)*0.1
                dEp = dEpold
                Ndold = Nd
                atom_en[0,0] = atom_en[0,0] - np.sign(dNd)*0.05
                atom_en[1,1] = atom_en[1,1] + np.sign(dNd)*0.05
                firstNdupdate = False
            else:
		sigma_iw_dat = np.squeeze(Sig_sym_iw.data)
                G0_2dlatt = G0_square_lattice(100, atom_en, t_hop)
                dN_dEnew, dNdE_mat = calc_dN_dE(beta, Nomeg, sigma_iw_dat, xmu, G0_2dlatt, EFFICIENT = True)
                del G0_2dlatt
                dNdEp = 2.*(-dNdE_mat[0,0].real + dNdE_mat[0,1].real) #Factor 2 to aid convergence. Attention, gives complex output!!!  
                dEp = dNd/dNdEp #dEpold/(Nd-Ndold)
                atom_en[0,0] = atom_en[0,0] - dEp #*0.5
                atom_en[1,1] = atom_en[1,1] + dEp #*0.5
                dEpold = dEp
                Ndold = Nd
            mpi.report('Ed = ' + str(atom_en[0,0]) + ' Ep = ' + str(atom_en[1,1]) + ' dEp = ' + str(dEp) + 'dNdEp = ' + str(dNdEp))    
            Elevel_update = True 
        else:
            Elevel_update = False
            mpi.report('Ed = ' + str(atom_en[0,0]) + ' Ep = ' + str(atom_en[1,1]) + ' dEp = 0')    

        
        # Calculate G0 for solver, using Slave Rotor representation and basis rotation
        mpi.report('New xmu = ', xmu)
        if (i_loop % 5) == 0: ALLOW_SR_UPDATE = True
        else: ALLOW_SR_UPDATE = False
        G0_params = beta, Nomeg, Ntau, xmu, Nfill_imp, Nfill_end, Udp, atom_en, t_hop, Sig_sym_iw, G_backtilde, filename, cos_exp_old, Ekin, GX_tau_old, G0_sol, ALLOW_SR_UPDATE
        Gd_solver, G0_solver, Delta_SR, Delta_init, G0_initial, GX_tau, cos_theta_exp, Ekin, SR_UPDATE = calculate_G0_SR_UdpUpp(G0_params, METHOD_NAME, mpirank)
        # Update GX_tau_old and use it to make convergence more stable
        GX_tau_old = GX_tau
        
        cos_theta_list.append(cos_theta_exp)
        Ekin_latt_list.append(Ekin)
        
        # Set the interacting part of the local Hamiltonian
        mpi.report('Nfill - Nfill_end = '+ str(np.abs(Nfill_imp - Nfill_end)))
        if np.abs(Nfill_imp - Nfill_end) < Nfill_end*relative_error:
            mpi.report('Update Nd_comp and Np_comp!')
            Nd_comp = Nd
            Np_comp = Np
            if n_cycles[0] < 1.2e6:
                n_cycles[0] = int(n_cycles[0]*2)
                n_cycles[1] = int(n_cycles[1]*1.5)
                
            relative_error = relative_error*0.5
            mpi.report('New N of cycles = '+str(n_cycles) + ' New maximal relative error = '+ str(relative_error))

        p["n_warmup_cycles"] = n_cycles[1]     # Number of warmup cycles to equilibrate
        p["n_cycles"] = n_cycles[0]          # Number of measurements
        
        for name, g0 in S.G0_iw:
            g0 << Gd_solver
    
    #mpi.report('Local Hamiltonian printout : ', S.h_loc, ' mu : ', xmu) 
    # Solve the impurity problem for the given interacting Hamiltonian and set of parameters
    S.solve(h_int = Hint, **p)
    
    ################### PROCESS RESULTS #################
    gs_iw = GfImFreq(indices = [0], beta=beta, n_points = Nomeg)

    # Impose paramagnetism
    gs_iw << (S.G_iw['up'] + S.G_iw['down'])/2.
            
    G_back_full = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)
    G_backtilde = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)
    G0_inverse = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)
    G0_inverse << inverse(G0_solver)
        
    #### The ctqmc algorithm only took a single orbital - now we transform back to d and p orbitals 
    G_backtilde['d','d'] << gs_iw['0','0']
    G_backtilde['d','p'] << gs_iw['0','0']*inverse(G0_inverse['p','p'])*(atom_en[1,0] + Delta_SR['p','d'])
    G_backtilde['p','d'] << gs_iw['0','0']*inverse(G0_inverse['p','p'])*(atom_en[0,1] + Delta_SR['d','p'])
    G_backtilde['p','p'] << inverse(G0_inverse['p','p']) + gs_iw['0','0']*inverse(G0_inverse['p','p'])*inverse(G0_inverse['p','p'])*(atom_en[1,0] + Delta_SR['p','d'])*(atom_en[0,1] + Delta_SR['d','p'])

    #### ... and transform to the tau axis
    G_back_tau = GfImTime(indices=['d','p'], beta=beta, n_points = Ntau)
    G_back_tau << InverseFourier(G_backtilde) #, fixed_coeff)

    G_SR_tau = GfImTime(indices=['d','p'], beta=beta, n_points = Ntau)
    
    # If SLAVE_ROT = True, multiply with Rotor Green's function, otherwise apply only shell folding
    if (METHOD_NAME == "Slave_Rotor_fully_const"):
        mpi.report('Only multiply Greens function by <cos theta>2')
        G_SR_tau.data[:,0,0] = G_back_tau.data[:,0,0]
        G_SR_tau.data[:,0,1] = G_back_tau.data[:,0,1]
        G_SR_tau.data[:,1,0] = G_back_tau.data[:,1,0]
        G_SR_tau.data[:,1,1] = G_back_tau.data[:,1,1]
    else:
        mpi.report('Multiply Greens function with full GX')
        G_SR_tau.data[:,0,0] = G_back_tau.data[:,0,0]*GX_tau
        G_SR_tau.data[:,0,1] = G_back_tau.data[:,0,1]*GX_tau
        G_SR_tau.data[:,1,0] = G_back_tau.data[:,1,0]*GX_tau
        G_SR_tau.data[:,1,1] = G_back_tau.data[:,1,1]*GX_tau
        
    #### The Physical Greens function (Gf*GX) on the Matsubara Axis 
    G_back_full << Fourier(G_SR_tau)
        
    #### Calculate Self Energy ####
    Sig_sym_iw = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)
    for name, g in S.G_iw: 
        Sig_sym_iw << Sig_sym_iw + 0.5*(inverse(G0_initial) - inverse(G_back_full)) #S.G0_iw[name]
       
    #### Calculate filling
    rho = S.density_matrix
    h_loc_diag = S.h_loc_diagonalization
    Nd = trace_rho_op(rho, op.n('up', 0)+op.n('down', 0), h_loc_diag)
    
    fixed_coeff = np.array([[[0.,0.],[0., 0.]], [[1.,0.],[0., 1.]]], dtype=complex)
    t1, e1 = G_back_full.fit_tail_on_window(int(Nomeg*0.3), int(Nomeg*0.7), fixed_coeff, 3,3)
    G_back_full.replace_by_tail(t1, int(Nomeg*0.7))
    
    Nd_tau = 2.*G_back_full.density()[0,0].real
    Nfill_tau = 2.*np.trace(G_back_full.density()).real
    Np = 2.*G_back_full.density()[1,1].real    
    Ndp = 2.*G_back_full.density()[0,1].real
    Npd = 2.*G_back_full.density()[1,0].real

    Nfill_imp = Nd + Np
    
    average_energyshift = ((Ed + Udp*Np - atom_en[0,0]) + (Ep + Udp*Nd + Up*Np/2. - atom_en[1,1]))/2.

    mpi.report('Use mixing factor ' + str(mixen_fac))
    mpi.report('Ed eff = ' +str(atom_en[0,0]) + ' Ep eff = ' + str(atom_en[1,1]) + ' Edp eff = ' + str(atom_en[0,1]) + ' Epd eff = ' + str(atom_en[1,0]))


    mpi.report('Nfill = ', Nfill_imp, 'Nfill_tau =', Nfill_tau)
    mpi.report('Nd trace = ', Nd, ' Nd tau = ', Nd_tau)
    mpi.report('xmu = ', xmu)
    
    if fixmu:
        print("mu is fixed - stop iteration!")
        maxerr = 0.
    else:
        maxerr = np.abs(Nd+Np - Nfill_end)
        
    if ((i_loop%2 == 0 ) or (i_loop > 10)) & (maxerr < 0.05):
        n_mu_vec[n_mu_ind,:] = np.array([xmu, Nd+Np])
        weightvec[n_mu_ind] = np.sqrt(n_cycles[0])
        n_mu_ind = n_mu_ind + 1
    
    ############## Save quantities of interest on the master node to an h5 archive ##############
    if  mpi.is_master_node():

        print('MASTER NODE, nth iteration: ', i_loop)
        with HDFArchive(filename,'a') as Results:
            Results['G0_iw-%s'%(i_loop+Noldit)] = G0_initial #S.G0_iw
            Results['G_iw-%s'%(i_loop+Noldit)] = G_back_full
            Results['Gtilde_iw-%s'%(i_loop+Noldit)] = G_backtilde
            #Results['Sigma_iw-%s'%(i_loop+Noldit)] = S.Sigma_iw
            Results['Delta_SR-%s'%(i_loop+Noldit)] = Delta_SR
            Results['Gdtilde_tau-%s'%(i_loop+Noldit)] = S.G_tau
            Results['Gtilde_tau-%s'%(i_loop+Noldit)] = G_back_tau
            Results['GSR_tau-%s'%(i_loop+Noldit)] = G_SR_tau
            Results['Delta_init-%s'%(i_loop+Noldit)] = Delta_init
            Results['Sigsym_iw-%s'%(i_loop+Noldit)] = Sig_sym_iw
            Results['Density-%s'%(i_loop+Noldit)] = S.density_matrix
            Results['GX_tau-%s'%(i_loop+Noldit)] = GX_tau
            Results['cos_theta_list'] = cos_theta_list
            Results['Ekin_latt_list'] = Ekin_latt_list

        of = open(densfile, "a")
        of.write('# '+str(i_loop+Noldit)+'th Iteration, Ntot = ' + str(Nd+Np) + '\n')
        of.write(str(Nd+Np) + ' ' + str(Nd) + ' '+ str(Np) + '\n')
        of.close()
    i_loop = i_loop +1 
    
