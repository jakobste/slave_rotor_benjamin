import numpy as np
from numpy import linalg as LA
from pytriqs.gf import *
import pytriqs.operators.util as op


def transf_Delta_phi(Delta_init, E_atom, U_4ind, phi, params):
    beta, Nomeg = params
    #w,U = LA.eig(atom_en) #Delta0)
    U = np.array([[np.cos(phi), np.sin(phi)],[-np.sin(phi), np.cos(phi)]])
    Ua = (U[0,0]+U[1,1])/2.
    Ub = (U[0,1]-U[1,0])/2.
    U[0,0] = Ua
    U[1,1] = Ua
    U[0,1] = Ub
    U[1,0] =-Ub

    Ut = np.conjugate(U.T)
    print('U = ', U, 'Ut = ', Ut)

    Delta_trans = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)
    ######### TRANSFORM SUCH THAT Delta_trans = Ut*Delta_init*U 
    Delta_trans['d','d'] << U[0,0]*(Ut[0,0]*Delta_init['d','d'] + Ut[0,1]*Delta_init['p','d']) \
        + U[1,0]*(Ut[0,0]*Delta_init['d','p'] + Ut[0,1]*Delta_init['p','p'])
    Delta_trans['d','p'] << U[0,1]*(Ut[0,0]*Delta_init['d','d'] + Ut[0,1]*Delta_init['p','d']) \
        + U[1,1]*(Ut[0,0]*Delta_init['d','p'] + Ut[0,1]*Delta_init['p','p'])   
    Delta_trans['p','d'] << U[0,0]*(Ut[1,0]*Delta_init['d','d'] + Ut[1,1]*Delta_init['p','d']) \
        + U[1,0]*(Ut[1,0]*Delta_init['d','p'] + Ut[1,1]*Delta_init['p','p'])   
    Delta_trans['p','p'] << U[0,1]*(Ut[1,0]*Delta_init['d','d'] + Ut[1,1]*Delta_init['p','d']) \
        + U[1,1]*(Ut[1,0]*Delta_init['d','p'] + Ut[1,1]*Delta_init['p','p']) 
        
    atom_en_rot = Ut.dot(E_atom).dot(U)
    
    # Transform the U-matrix for the problem
    Ut_4ind = op.transform_U_matrix(U_4ind, U.T)
    
    return Delta_trans, atom_en_rot, Ut_4ind

def transf_Delta_auto(Delta_init, E_atom, U_4ind, params):
    beta, Nomeg = params
    Nomeg2 = Delta_init.data[:,0,0].size
    #assert Nomeg == Nomeg2, "Nomeg given as parameter does not correspond to actual number of Matsubara frequencies!\n"
    print('Delta_init.data[:,0,0].size = ', Nomeg2, ', Nomeg = ', Nomeg)    

    Delta0 = Delta_init.data[Nomeg/2 +1]
    w,U = LA.eig(Delta0)
    w2,U2 = LA.eig(E_atom) #Delta0)
    w3,U3 = LA.eig(E_atom + Delta0)
    phi = np.arcsin(U[0,1].real)
    phi2 = np.arcsin(U2[0,1].real)
    phi3 = np.arcsin(U3[0,1].real)
    print('phi Delta0', phi, ' phi E_atom', phi2, 'phi E_atom + Delta0 ', phi3)
    U = np.array([[np.cos(phi), np.sin(phi)],[-np.sin(phi), np.cos(phi)]])
    print('Umat phi ', U)
    Ua = (U[0,0]+U[1,1])/2.
    Ub = (U[0,1]-U[1,0])/2.
    U[0,0] = Ua
    U[1,1] = Ua
    U[0,1] = Ub
    U[1,0] =-Ub

    Ut = np.conjugate(U.T)
    print('U = ', U, 'Ut = ', Ut)

    Delta_trans = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)
    ######### TRANSFORM SUCH THAT Delta_trans = Ut*Delta_init*U 
    Delta_trans['d','d'] << U[0,0]*(Ut[0,0]*Delta_init['d','d'] + Ut[0,1]*Delta_init['p','d']) \
        + U[1,0]*(Ut[0,0]*Delta_init['d','p'] + Ut[0,1]*Delta_init['p','p'])
    Delta_trans['d','p'] << U[0,1]*(Ut[0,0]*Delta_init['d','d'] + Ut[0,1]*Delta_init['p','d']) \
        + U[1,1]*(Ut[0,0]*Delta_init['d','p'] + Ut[0,1]*Delta_init['p','p'])   
    Delta_trans['p','d'] << U[0,0]*(Ut[1,0]*Delta_init['d','d'] + Ut[1,1]*Delta_init['p','d']) \
        + U[1,0]*(Ut[1,0]*Delta_init['d','p'] + Ut[1,1]*Delta_init['p','p'])   
    Delta_trans['p','p'] << U[0,1]*(Ut[1,0]*Delta_init['d','d'] + Ut[1,1]*Delta_init['p','d']) \
        + U[1,1]*(Ut[1,0]*Delta_init['d','p'] + Ut[1,1]*Delta_init['p','p']) 
        
    atom_en_rot = Ut.dot(E_atom).dot(U)
    
    # Transform the U-matrix for the problem
    Ut_4ind = op.transform_U_matrix(U_4ind, U.T)
    
    return Delta_trans, atom_en_rot, Ut_4ind, phi
