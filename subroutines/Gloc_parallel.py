import numpy as np
from pytriqs.gf import *
from Sigma_Model_Greensfunction import *

#################### GLOC FOR 3D SQUARE LATTICE ############################
#ngrid = 100
#Vol = ngrid**2

#kmeshx, kmeshy = np.meshgrid((np.arange(ngrid)+0.5)*np.pi/ngrid,(np.arange(ngrid)+0.5)*np.pi/ngrid)

class G0_square_lattice:
    
    def __init__(self, ngrd, at_en, t_h):
        self.ngrid = ngrd
        self.Vol = ngrd**2
        self.ngrid_big = ngrd
        self.ngrid_small = ngrd
        
        self.kmeshx, self.kmeshy = np.meshgrid((np.arange(ngrd)+0.5)*np.pi/ngrd,(np.arange(ngrd)+0.5)*np.pi/ngrd)
        self.cos_mesh = 2.*(np.cos(self.kmeshx)+np.cos(self.kmeshy))
        self.kmeshx_small, self.kmeshy_small = np.meshgrid((np.arange(ngrd)+0.5)*np.pi/ngrd,(np.arange(ngrd)+0.5)*np.pi/ngrd) # to calculate gradients
        self.cos_mesh_small = 2.*(np.cos(self.kmeshx_small)+np.cos(self.kmeshy_small)) # to calculate gradients
        self.kmeshx_big, self.kmeshy_big = np.meshgrid((np.arange(ngrd)+0.5)*np.pi/ngrd,(np.arange(ngrd)+0.5)*np.pi/ngrd) # to calculate gradients
        self.cos_mesh_big = 2.*(np.cos(self.kmeshx_small)+np.cos(self.kmeshy_small)) # to calculate gradients

        
        self.atom_en = at_en
        self.t_hop = t_h
        
    def __del__(self):
        print("Delete G0_square_lattice object")
        
    
    def update_hoppings(self, at_en, t_h):
        self.atom_en = at_en
        self.t_hop = t_h
        
    def update_kmesh_small(self, ngrd_small):
        self.kmeshx_small, self.kmeshy_small = np.meshgrid((np.arange(ngrd_small)+0.5)*np.pi/ngrd_small,(np.arange(ngrd_small)+0.5)*np.pi/ngrd_small) # to calculate gradients
        self.cos_mesh_small = 2.*(np.cos(self.kmeshx_small)+np.cos(self.kmeshy_small)) # to calculate gradients
        self.ngrid_small = ngrd_small
        
    def update_kmesh_big(self, ngrd_big):
        self.kmeshx_big, self.kmeshy_big = np.meshgrid((np.arange(ngrd_big)+0.5)*np.pi/ngrd_big,(np.arange(ngrd_big)+0.5)*np.pi/ngrd_big) # to calculate gradients
        self.cos_mesh_big = 2.*(np.cos(self.kmeshx_big)+np.cos(self.kmeshy_big)) # to calculate gradients
        self.ngrid_big = ngrd_big
        
    def Glociw(self, iw, sigmaiw, xmu):
    #cos_k = 2.*(np.cos(kmeshx)+np.cos(kmeshy)+np.cos(kmeshz)) # attention, kz is has already been subsituted: kz -> kz*a (a lattice const.)
        #cos_k = 2.*(np.cos(self.kmeshx)+np.cos(self.kmeshy))
    
        # matrix elements for 2x2 case
        a = xmu - self.atom_en[0,0] - self.t_hop[0,0]*self.cos_mesh - sigmaiw[0,0] # t_hop[0,0] = tdd
        a = a + iw # because of weird python cast -> otherwise not compatible 
        b = - self.atom_en[0,1] - self.t_hop[0,1]*self.cos_mesh - sigmaiw[0,1] # t_hop[0,1] = tdp
        c = - self.atom_en[1,0] - self.t_hop[1,0]*self.cos_mesh - sigmaiw[1,0]
        d = xmu - self.atom_en[1,1] - self.t_hop[1,1]*self.cos_mesh - sigmaiw[1,1] 
        d = d + iw # because of weird python cast -> otherwise not compatible 

        # integrated inverse matrix
        aout =  np.sum(d/(a*d-b*c))
        bout = -np.sum(b/(a*d-b*c))
        cout = -np.sum(c/(a*d-b*c))
        dout =  np.sum(a/(a*d-b*c))
    
        return np.array([[aout,bout],[cout,dout]])/self.Vol
    
    def N_mat_T0(self, xmu):
        print('Routine to calculate N at T=0. No self energy taken into account -> only Hamiltonian on 2D square lattice!')
        a = -xmu + self.atom_en[0,0] + self.t_hop[0,0]*self.cos_mesh_big
        b = self.atom_en[0,1] + self.t_hop[0,1]*self.cos_mesh_big
        c = self.atom_en[1,0] + self.t_hop[1,0]*self.cos_mesh_big
        d = -xmu + self.atom_en[1,1] + self.t_hop[1,1]*self.cos_mesh_big
        
        # Eigenvalues of 2x2 matrix
        lambd_0 = ((a+d) + np.sqrt((a-d)**2 +4*b*c))/2. 
        lambd_1 = ((a+d) - np.sqrt((a-d)**2 +4*b*c))/2. 
        
        templ_0 = lambd_0 < 0.
        templ_1 = lambd_1 < 0.
        
        nominator_0 = (b**2 + (lambd_0 - a)**2)
        nominator_1 = (b**2 + (lambd_1 - a)**2)
        
        # Define projector onto first eigenstate
        P0_00 = b**2/nominator_0
        P0_01 = b*(lambd_0 - a)/nominator_0
        P0_10 = b*(lambd_0 - a)/nominator_0
        P0_11 = (lambd_0 - a)**2/nominator_0
        
        # Define projector onto second eigenstate
        P1_00 = b**2/nominator_1
        P1_01 = b*(lambd_1 - a)/nominator_1
        P1_10 = b*(lambd_1 - a)/nominator_1
        P1_11 = (lambd_1 - a)**2/nominator_1
        
        
        Nd = np.sum(P0_00[templ_0]) + np.sum(P1_00[templ_1])
        Ndp = np.sum(P0_01[templ_0]) + np.sum(P1_01[templ_1])
        Npd = np.sum(P0_10[templ_0]) + np.sum(P1_10[templ_1])
        Np = np.sum(P0_11[templ_0]) + np.sum(P1_11[templ_1])
        
        return np.array([[Nd, Ndp],[Npd, Np]])/self.ngrid_big**2
    
    
    def dGlociw_dE_diag(self, iw, sigmaiw, xmu):
        # matrix elements for 2x2 case
        #print('xmu ', xmu.shape, ' self.atom_en[0,0] ', self.atom_en[0,0].shape)
        a = xmu - self.atom_en[0,0] - self.t_hop[0,0]*self.cos_mesh_small - sigmaiw[0,0] # t_hop[0,0] = tdd
        a = a + iw # because of weird python cast -> otherwise not compatible 
        b = - self.atom_en[0,1] - self.t_hop[0,1]*self.cos_mesh_small - sigmaiw[0,1] # t_hop[0,1] = tdp
        c = - self.atom_en[1,0] - self.t_hop[1,0]*self.cos_mesh_small - sigmaiw[1,0]
        d = xmu - self.atom_en[1,1] - self.t_hop[1,1]*self.cos_mesh_small - sigmaiw[1,1] 
        d = d + iw # because of weird python cast -> otherwise not compatible 
        
        # integrated inverse matrix
        daout_dE00 = np.sum((d/(a*d-b*c))**2)
        daout_dE11 = np.sum((d*a)/(a*d-b*c)**2 - 1./(a*d-b*c))
        ddout_dE00 = np.sum((a*d)/(a*d-b*c)**2 - 1./(a*d-b*c))
        ddout_dE11 = np.sum((a/(a*d-b*c))**2)
        
        return np.array([[daout_dE00,daout_dE11],[ddout_dE00,ddout_dE11]])/self.Vol
    
    
    def Ekiniw(self, iw, sigmaiw, xmu, cos_theta_exp):
        
        # ek matrix 
        ea = self.t_hop[0,0]*self.cos_mesh # t_hop[0,0] = tdd
        eb = self.t_hop[0,1]*self.cos_mesh # t_hop[0,1] = tdp
        ec = self.t_hop[1,0]*self.cos_mesh
        ed = self.t_hop[1,1]*self.cos_mesh
    
        # matrix elements for 2x2 case
        a = xmu - self.atom_en[0,0] - ea*(cos_theta_exp**2) - sigmaiw[0,0] 
        a = a + iw # because of weird python cast -> otherwise not compatible 
        b = - self.atom_en[0,1] - eb*(cos_theta_exp**2) - sigmaiw[0,1] 
        c = - self.atom_en[1,0] - ec*(cos_theta_exp**2) - sigmaiw[1,0]
        d = xmu - self.atom_en[1,1] - ed*(cos_theta_exp**2) - sigmaiw[1,1] 
        d = d + iw # because of weird python cast -> otherwise not compatible 
 

        # Attention, this is not a matrix multiplication -> in case of doubts, check the derivation 
        aout =  np.sum(ea*d/(a*d-b*c))
        bout = -np.sum(eb*b/(a*d-b*c))
        cout = -np.sum(ec*c/(a*d-b*c))
        dout =  np.sum(ed*a/(a*d-b*c))
        
#         # integrated inverse matrix (off diagonals)
#         bout_1 = -np.sum(ea*b/(a*d-b*c))
#         bout_2 =  np.sum(eb*a/(a*d-b*c))
#         cout_1 =  np.sum(ec*d/(a*d-b*c))
#         cout_2 = -np.sum(ed*c/(a*d-b*c))
    
        # first output is trace over second (matrix) output
        return (aout+bout+cout+dout)/self.Vol, np.array([[aout, bout],[cout, dout]])/self.Vol

    def Eksum(self):
        # ek matrix 
        ea = self.t_hop[0,0]*self.cos_mesh
        ed = self.t_hop[1,1]*self.cos_mesh
        
        esum_a = np.sum(ea)
        esum_d = np.sum(ed)
        
        return (esum_a+esum_d)/self.Vol
        
    def DOS(self, eps, eta):
        return -np.sum( (eps + 1j*eta - self.cos_mesh)**(-1)).imag/(np.pi*self.Vol)
    

def calc_G0_parallel(mpisize, mpirank, beta, Nomeg, sigma0, mu, G0_2dlatt):
    # create vector for iOmega
    iomegTr  = GfImFreq(indices=[0], beta=beta, n_points = Nomeg)
    iomegTr << iOmega_n
    iomeg    = np.squeeze(iomegTr.data)
    
    Nmatsub  = iomeg.size

    ist  = mpirank*int(Nmatsub/mpisize)
    iend = (mpirank+1)*int(Nmatsub/mpisize)
    if mpirank == (mpisize-1): iend = Nmatsub

    g0_single = np.empty((iend-ist,2,2), dtype = np.complex64)

    if mpirank == 0: print('istart, iend',ist,iend)
    for ind in np.arange(ist,iend):
        #if ind % 100 == 0: print(ind)
        g0_single[ind-ist,:,:] = G0_2dlatt.Glociw(iomeg[ind],sigma0, mu)
        #if mpirank == 0: print('index = ',ind)
   
    if mpirank == (mpisize-1):
        g0_tail = g0_single[int(Nmatsub/mpisize):,:,:]
        g0_single = g0_single[:int(Nmatsub/mpisize),:,:]
    else:
        g0_tail = None


    g0_gather = np.empty((mpisize*int(Nmatsub/mpisize),2,2), dtype = np.complex64)
    #g0_tail = comm.bcast(g0_tail,root = mpisize-1)
    #comm.Allgather([g0_single, mpi.MPI.DOUBLE],[g0_gather, mpi.MPI.DOUBLE])

    #g0_unnorm = np.concatenate((g0_gather,g0_tail),axis = 0)
    #mpi.report('SUM over all frequencies = '+ str(np.sum(g0_unnorm[:,0,0].real))+'+'+str(np.sum(g0_unnorm[:,0,0].imag)) + 'i')
    #G0_initial.data[:,:,:] = g0_unnorm

    return g0_gather, g0_tail, g0_single

def calc_G0(beta, Nomeg, sigma, mu, G0_2dlatt, EFFICIENT = False):
    # create vector for iOmega
    iomegTr  = GfImFreq(indices=[0], beta=beta, n_points = Nomeg)
    iomegTr << iOmega_n
    iomeg    = np.squeeze(iomegTr.data)
    
    Nmatsub  = iomeg.size

    G0_new = np.empty((Nmatsub,2,2), dtype = np.complex64)
    
    #print('sigma shape = ', sigma.shape, 'iomeg shape = ', iomeg.shape)

    if EFFICIENT:
        for ind in np.arange(Nomeg):
            #if ind % 100 == 0: print(ind)
            if Nomeg+ind == Nmatsub:
                print(Nomeg, ind)
            G0_new[Nomeg+ind,:,:] = G0_2dlatt.Glociw(iomeg[Nomeg+ind],sigma[Nomeg+ind,:,:], mu)
        
        G0_new[:Nomeg,:,:] = np.conj(G0_new[Nmatsub:Nomeg-1:-1,:,:])
    else:  
        for ind in np.arange(Nmatsub):
            #if ind % 100 == 0: print(ind)
            G0_new[ind,:,:] = G0_2dlatt.Glociw(iomeg[ind],sigma[ind,:,:], mu)
    
    # Factor 2 because summation was only over half of the Matsubara frequencies
    Ndp = 2*np.sum(G0_new[Nomeg:,:,:], axis=0).real/beta  + 0.5
    return G0_new, Ndp



################## CALCULATE DERIVATIVES OF Nd and Np AS FUNCTIONS OF Ed AND EP ##########################
# (tested!!!)
def calc_dN_dE(beta, Nomeg, sigma, mu, G0_2dlatt, EFFICIENT = False):
    # create vector for iOmega
    iomegTr  = GfImFreq(indices=[0], beta=beta, n_points = Nomeg)
    iomegTr << iOmega_n
    iomeg    = np.squeeze(iomegTr.data)
    
    Nmatsub  = iomeg.size

    dN_dEnew = np.empty((Nmatsub,2,2), dtype = np.complex64)
    
    #print('sigma shape = ', sigma.shape, 'iomeg shape = ', iomeg.shape)
    if EFFICIENT:
        for ind in np.arange(Nomeg):
            dN_dEnew[Nomeg+ind,:,:] = G0_2dlatt.dGlociw_dE_diag(iomeg[Nomeg+ind], sigma[Nomeg+ind,:,:], mu)
        
        dN_dEnew[:Nomeg,:,:] = np.conj(dN_dEnew[Nmatsub:Nomeg-1:-1,:,:])
    else:
        for ind in np.arange(Nmatsub):
            #if ind % 100 == 0: print(ind)
            dN_dEnew[ind,:,:] = G0_2dlatt.dGlociw_dE_diag(iomeg[ind],sigma[ind,:,:], mu)
    
    dNdE_mat = 2*np.sum(dN_dEnew[Nomeg:,:,:], axis = 0).real/beta
    return dN_dEnew, dNdE_mat


def calc_Elatt(beta, Nomeg, sigma_iw, cos_theta_exp, xmu, eta, G0_2dlatt, MATRIX_OUT = False):
    
    # create vector for iOmega
    iomegTr  = GfImFreq(indices=[0], beta=beta, n_points = Nomeg)
    iomegTr << iOmega_n
    iomeg    = np.squeeze(iomegTr.data)
    
    Nmatsub  = iomeg.size
    #print('Nmatsub = ', Nmatsub)
    
    Eksum = G0_2dlatt.Eksum()
    
    if MATRIX_OUT:
        E_sum = np.zeros((2,2))
        for ind, omeg in enumerate(iomeg[Nmatsub/2:]):
            E_val, E_val_mat = G0_2dlatt.Ekiniw(omeg, sigma_iw[Nmatsub/2 + ind,:,:], xmu, cos_theta_exp)
            E_sum = E_sum + 2*(E_val_mat*np.exp(-omeg*eta)).real
            
    else:
        E_sum = 0.
        for ind, omeg in enumerate(iomeg[Nmatsub/2:]):
            E_val, E_val_mat = G0_2dlatt.Ekiniw(omeg, sigma_iw[Nmatsub/2 + ind,:,:], xmu, cos_theta_exp)
            E_sum = E_sum + 2*(E_val*np.exp(-omeg*eta)).real

    return E_sum/beta 

def calc_Elatt_DMFT(beta, Nomeg, Delta_iw, G_imp, cos_theta_exp, eta):
    # create vector for iOmega
    iomegTr  = GfImFreq(indices=[0], beta=beta, n_points = Nomeg)
    iomegTr << iOmega_n
    iomeg    = np.squeeze(iomegTr.data)
    
    Delt_dat = np.squeeze(Delta_iw.data)
    G_dat = np.squeeze(G_imp.data)
    
    Nmatsub  = iomeg.size
    
    E_0 = 2.*np.sum(Delt_dat[Nmatsub/2:,0,0]*G_dat[Nmatsub/2:,0,0]*np.exp(-iomeg[Nmatsub/2:]*eta)).real
    E_1 = 2.*np.sum(Delt_dat[Nmatsub/2:,0,1]*G_dat[Nmatsub/2:,0,1]*np.exp(-iomeg[Nmatsub/2:]*eta)).real
    E_2 = 2.*np.sum(Delt_dat[Nmatsub/2:,1,0]*G_dat[Nmatsub/2:,1,0]*np.exp(-iomeg[Nmatsub/2:]*eta)).real
    E_3 = 2.*np.sum(Delt_dat[Nmatsub/2:,1,1]*G_dat[Nmatsub/2:,1,1]*np.exp(-iomeg[Nmatsub/2:]*eta)).real
    E_sum = E_0+E_1+E_2+E_3

    return E_sum/(beta*cos_theta_exp**2) 

def calc_Eimp_DMFT(beta, Nomeg, Delta_iw, G_imp, eta):
    # create vector for iOmega
    iomegTr  = GfImFreq(indices=[0], beta=beta, n_points = Nomeg)
    iomegTr << iOmega_n
    iomeg    = np.squeeze(iomegTr.data)
    
    Delt_dat = np.squeeze(Delta_iw.data)
    G_dat = np.squeeze(G_imp.data)
    
    Nmatsub  = iomeg.size
    
    E_0 = 2.*np.sum(Delt_dat[Nmatsub/2:,0,0]*G_dat[Nmatsub/2:,0,0]*np.exp(-iomeg[Nmatsub/2:]*eta)).real
    E_1 = 2.*np.sum(Delt_dat[Nmatsub/2:,0,1]*G_dat[Nmatsub/2:,0,1]*np.exp(-iomeg[Nmatsub/2:]*eta)).real
    E_2 = 2.*np.sum(Delt_dat[Nmatsub/2:,1,0]*G_dat[Nmatsub/2:,1,0]*np.exp(-iomeg[Nmatsub/2:]*eta)).real
    E_3 = 2.*np.sum(Delt_dat[Nmatsub/2:,1,1]*G_dat[Nmatsub/2:,1,1]*np.exp(-iomeg[Nmatsub/2:]*eta)).real
    E_sum = E_0+E_1+E_2+E_3

    return E_sum/beta

def calc_GX_sigma_DMFT(params):
    
    beta, Nomeg, Nfill, Norbs, Udp, Delta_tau, G_imp_tau = params
    
    # here G(\tau)*Delta(-\tau) and DeltaX_{\nu} instead of G(-\tau)*Delta(\tau) and DeltaX_{-\nu} 
    # as in documentation!
    DX_1 = np.squeeze(G_imp_tau.data)[:,0,0]*np.squeeze(Delta_tau.data)[::-1,0,0]
    DX_2 = np.squeeze(G_imp_tau.data)[:,0,1]*np.squeeze(Delta_tau.data)[::-1,0,1]
    DX_3 = np.squeeze(G_imp_tau.data)[:,1,0]*np.squeeze(Delta_tau.data)[::-1,1,0]
    DX_4 = np.squeeze(G_imp_tau.data)[:,1,1]*np.squeeze(Delta_tau.data)[::-1,1,1]
    
    # Factor 2 because of 2 spins
    DeltaX_tau = 2.*(DX_1+DX_2+DX_3+DX_4)
    
    # Make bosonic GF object and with data from above and fourier transform it
    pars0 = [beta, Nomeg, "time"]
    DeltaX = GreensFunction_boson(pars0)
    DeltaX.set_GX(DeltaX_tau)
    DeltaX.Fourier()

    DeltaX_nu = DeltaX.GX_data()
    
    # Initial guesses for h0 and l0 (from case with DeltaX = 0)
    h0 = (Nfill - np.round(Nfill))*Udp/2. # Should work for commensurate fillings #(Nfill - Norbs)*Udp/2. 
    l0 = Udp/2.
    
    pars1 = [beta, Nomeg, Udp, h0, l0, DeltaX_nu]
    GX = GreensFunction_boson(pars1)
    
    
    # Enforce constraints with dichotomy method (saver!)
    if h0 == 0.:
        lout = GX.enfore_GX0_dichotomy()
        hout = 0.
        print('At commensurate filling!')
    else:
        lout, hout = GX.enforce_constraint_dichotomy(Nfill, np.round(Nfill)) # Should work for commensurate fillings
        print('At incommensurate filling!')

    # give back GX in nu and tau space
    GX_nu = GX.GX_data()
    GX.Inverse_Fourier()
    GX_tau = GX.GX_data()
    tau = GX.tau()
    
    return GX_nu, GX_tau, lout, hout
