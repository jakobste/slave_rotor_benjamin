import numpy as np
from pytriqs.gf import *
from scipy.interpolate import interp1d
from scipy import arange, array, exp

def extrap1d(interpolator):
    xs = interpolator.x
    ys = interpolator.y

    def pointwise(x):
        if x < xs[0]:
            return ys[0]+(x-xs[0])*(ys[1]-ys[0])/(xs[1]-xs[0])
        elif x > xs[-1]:
            return ys[-1]+(x-xs[-1])*(ys[-1]-ys[-2])/(xs[-1]-xs[-2])
        else:
            return interpolator(x)

    def ufunclike(xs):
        return array(map(pointwise, array(xs)))

    return ufunclike

def change_temperature(betanew, Npoints, Gfunc):
    Gdat = np.squeeze(Gfunc.data)
    siz = int(Gdat.size/4)
    betaold = Gfunc.mesh.beta
    omegvec = (np.arange(siz/2)*2+1)*np.pi/betaold
    omegvecnew = (np.arange(Npoints)*2+1)*np.pi/betanew
    
    if omegvecnew[-1] > omegvec[-1]+np.pi/betanew:
        print('ERROR IN <<CHANGE_TEMPERATURE>> SUBROUTINE !!! new omega range bigger than old one - no intrapolation possible!!!')
    
    f3dd = interp1d(omegvec, Gdat[siz/2:,0,0], kind = 'cubic')
    f3dp = interp1d(omegvec, Gdat[siz/2:,0,1], kind = 'cubic')
    f3pd = interp1d(omegvec, Gdat[siz/2:,1,0], kind = 'cubic')
    f3pp = interp1d(omegvec, Gdat[siz/2:,1,1], kind = 'cubic')
    fextdd = extrap1d(f3dd)
    fextdp = extrap1d(f3dp)
    fextpd = extrap1d(f3pd)
    fextpp = extrap1d(f3pp)
 
    Gfunc_new = GfImFreq(indices=['d','p'], beta=betanew, n_points = Npoints)
    
    Gfunc_new.data[Npoints:,0,0] = fextdd(omegvecnew)
    Gfunc_new.data[:Npoints,0,0] = np.conj(np.flip(fextdd(omegvecnew), 0))
    Gfunc_new.data[Npoints:,0,1] = fextdp(omegvecnew)
    Gfunc_new.data[:Npoints,0,1] = np.conj(np.flip(fextdp(omegvecnew), 0))
    Gfunc_new.data[Npoints:,1,0] = fextpd(omegvecnew)
    Gfunc_new.data[:Npoints,1,0] = np.conj(np.flip(fextpd(omegvecnew), 0))
    Gfunc_new.data[Npoints:,1,1] = fextpp(omegvecnew)
    Gfunc_new.data[:Npoints,1,1] = np.conj(np.flip(fextpp(omegvecnew), 0))
    
    return Gfunc_new
