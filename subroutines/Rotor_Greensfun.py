import numpy as np

def Rotor_Greens(params):
    beta, Udp, h, Ntau = params
    PI = np.pi    

    #print(beta, Udp, h, Ntau)
    Nrot = 100
    
    tauvec = (np.arange(float(Ntau))/float(Ntau))*beta 

    if np.abs(Udp) == 0:
        G0X = tauvec*0 + 1.
        dG0dt = 0.
    else:
        N = 4.
        # summing over the winding number
        cos_exp_sum = tauvec*0. + 1.
        sin_exp_sum = tauvec*0.
        for n in range(1,Nrot):
            cos_exp_sum = cos_exp_sum + 2.*np.cos(2.*n*PI*(h/Udp - N/2. +tauvec/beta))*np.exp(-2.*(n*PI)**2/(Udp*beta))
            sin_exp_sum = sin_exp_sum + 2.*(2.*n*PI)*np.sin(2.*n*PI*h/Udp)*np.exp(-2.*(n*PI)**2/(Udp*beta))/beta

        # matsubara sum in exponent
        omegvec = np.arange(1,float(Ntau+1))*2.*PI/beta
        
        omxx, tauyy = np.meshgrid(omegvec, tauvec)
        exp_mat = (omxx**(-2))*np.cos(omxx*tauyy)
        exp_sum = exp_mat.sum(axis=1) # should correspond to \sum (1/\omeg^2)cos(\omeg\tau)

        exponent = 2.*(exp_sum - exp_sum[0])*Udp/beta # should correspond to \sum (Udp/\beta\omeg^2)(cos(\omeg\tau)-1)

        G0X = (cos_exp_sum/cos_exp_sum[0])*np.exp(exponent)
        dG0dt = sin_exp_sum/cos_exp_sum[0]
    
    return G0X, dG0dt, tauvec

def calc_dG0dt(params):
    beta, Udp, h, Ntau = params
    PI = np.pi    
    
    if np.abs(Udp) == 0:
        dG0dt = 0.
    else:
        Nrot = 1000

        cos_exp_sum = 1.
        sin_exp_sum = 0.
        for n in range(1,Nrot):
            cos_exp_sum = cos_exp_sum + 2.*np.cos(2.*n*PI*h/Udp)*np.exp(-2.*(n*PI)**2/(Udp*beta))
            sin_exp_sum = sin_exp_sum + 2.*(2.*n*PI)*np.sin(2.*n*PI*h/Udp)*np.exp(-2.*(n*PI)**2/(Udp*beta))/beta

        dG0dt = -sin_exp_sum/cos_exp_sum # minus sign because dcos(x)/dx = - sin(x)

    return dG0dt

def Rotor_Greens_fixed_N(params):
    beta, Udp, Nimp, Ntau = params 
    
    if np.abs(Udp) == 0.:
        tauvec = (np.arange(Ntau, dtype=float)/Ntau)*beta 
        G0X = tauvec*0 + 1
        dG0dt = 0.
        h_old = 0.
        
    else:
        h_old = 0.
        dh = 0.01
        #params[2] = h_old + dh
        dG0dt_old = 0.
        params = [beta, Udp, h_old + dh, Ntau]
        dG0dt_new = calc_dG0dt(params)

        delth = (Nimp-2)*Udp/(dG0dt_new/dh + 1.)
        while np.abs(delth) > 1e-7:
            params[2] = h_old + delth  
            dG0dt_new = calc_dG0dt(params)
            params[2] = h_old + delth*(1.01) 
            dG0dt_new_dh = calc_dG0dt(params)
            h_old = h_old + delth

            dGdh = (dG0dt_new_dh - dG0dt_new)/(delth*0.01)
            delth = -((Nimp-2.)*Udp + dG0dt_new + h_old)/(dGdh + 1.)
            #print('delth =', delth, ' dG0dt_new ', dG0dt_new, ' dGdh ', dGdh)
            dG0dt_old = dG0dt_new

        params[2] = h_old
        G0X, dG0dt, tauvec = Rotor_Greens(params)
    
    return G0X, dG0dt, tauvec, h_old
    