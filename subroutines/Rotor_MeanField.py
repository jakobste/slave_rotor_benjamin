import numpy as np
from numpy import linalg as LA


def expectation_value(operat, eigval, eigvec, T):
    exp_nonorm = T*0.
    norm = 0.
    opdim = operat.shape[0]
    mineig = np.amin(eigval)
    for n in np.arange(eigval.size):
        weight = np.exp(-(eigval[n]-mineig)/T)
        matel = (np.conjugate(eigvec[n]).reshape(1,opdim)).dot(operat).dot(eigvec[n].reshape(opdim,1))
        exp_nonorm = exp_nonorm + weight*matel
        norm = norm + weight
    return np.asscalar(exp_nonorm/norm)


class Lehmann_greensfun:
    def __init__(self, operat, eigval, eigvec, beta):
        opdim = operat.shape[0]
        self.Neig  = eigval.size
        self.beta  = beta
        self.eig_vals = eigval
        self.min_eig = np.amin(eigval)
        self.MatEl = np.zeros((self.Neig, self.Neig))
        for n in range(self.Neig):
            for m in range(self.Neig):
                self.MatEl[m,n] = (np.conjugate(eigvec[m]).reshape(1,opdim)).dot(operat).dot(eigvec[n].reshape(opdim,1))
        
        # renormalized partition sum (renormalized by minimal eigenvalue)
        self.Z_ren = np.sum(np.exp(-beta*(eigval - self.min_eig)))
    
    def print_matrix(self):
        return self.MatEl
        
    def check_sum_rule(self):
        sum_mn = 0.
        for n in range(self.Neig):
            for m in range(self.Neig):
                sum_mn = sum_mn + np.abs(self.MatEl[m,n])**2.*(np.exp(-self.beta*(self.eig_vals[n]- self.min_eig)) + np.exp(-self.beta*(self.eig_vals[m]- self.min_eig)))
        return sum_mn/self.Z_ren
    
    def Greensfunction(self, tau):
        # Formally, both expressions are equivalent -> but splitting should facilitate numerics!
        #if (tau < self.beta/2.):
        bravec = (np.exp(-self.beta*(self.eig_vals - self.min_eig) + tau*self.eig_vals)).reshape(1,self.Neig)
        ketvec = (np.exp(-tau*self.eig_vals)).reshape(self.Neig, 1)
#         else:
#             bravec = (np.exp(-self.beta*self.eig_vals + tau*self.eig_vals)).reshape(1,self.Neig)
#             ketvec = (np.exp(self.beta*self.min_eig - tau*self.eig_vals)).reshape(self.Neig, 1)
        
        Gtau = bravec.dot(np.abs(self.MatEl)**2).dot(ketvec)/self.Z_ren
        
        return Gtau
        
    def Greensfunction_omeg(self, omeg, eta):
        # Formally, both expressions are equivalent -> but splitting should facilitate numerics!
        #if (tau < self.beta/2.):
        G_om = 0.
        for n in range(self.Neig):
            for m in range(self.Neig):
                G_om = G_om + np.abs(self.MatEl[m,n])**2.*(np.exp(-self.beta*(self.eig_vals[n]- self.min_eig)) + np.exp(-self.beta*(self.eig_vals[m]- self.min_eig)))/(omeg + self.eig_vals[n] - self.eig_vals[m] + 1j*eta)
        
        return G_om/self.Z_ren
    
    def __del__(self):
        print "Delete Lehmann_grennsfun object"
    

#### time evolution causes problems ! ####
def time_evolution(operat, eigval, eigvec, tau):
    opdim = operat.shape[0]
    time_evol_mat = np.zeros((opdim,opdim))
    time_evol_mat_inv = np.zeros((opdim,opdim))
    for n in np.arange(opdim):
        weight = np.exp(-eigval[n]*tau)
        evec_mat = (eigvec[n].reshape(opdim,1)).dot(np.conjugate(eigvec[n]).reshape(1,opdim))  
        time_evol_mat = time_evol_mat + weight*evec_mat
        time_evol_mat_inv = time_evol_mat_inv + evec_mat/weight
    
    operat_tau = time_evol_mat.dot(operat).dot(time_evol_mat_inv)
    return operat_tau

def build_cosmat_Nmat(NN):
    Nmat = 2*NN+1

    kdiag = np.arange(Nmat)-(Nmat-1)/2
    
    cosmat = np.zeros((kdiag.size,kdiag.size))

    for i in np.arange(Nmat-1):
        cosmat[i,i+1] = 1/2.
        cosmat[i+1,i] = 1/2.
        
    Nmat = np.diag(kdiag)
    
    return cosmat, Nmat
    
def build_L2mat(NN):
    Nmat = 2*NN+1

    kdiag = np.arange(Nmat)-(Nmat-1)/2
    
    L2mat = np.diag(kdiag**2)
    return L2mat

def build_sinmat_Nmat(NN):
    Nmat = 2*NN+1

    kdiag = np.arange(Nmat)-(Nmat-1)/2

    sinmat = np.zeros((kdiag.size,kdiag.size), dtype = complex)

    for i in np.arange(Nmat-1):
        sinmat[i,i+1] = -1j/2.
        sinmat[i+1,i] = 1j/2.

    Nmat = np.diag(kdiag)

    return sinmat, Nmat
    
    
def build_H_N(cosmat, U, K, h, NN):
    #print('Now calculate N-Hamiltonian!')
    
    Nmat = 2*NN+1

    kdiag = np.arange(Nmat)-(Nmat-1)/2
    
    diagH = (U/2.*kdiag**2. + h*kdiag) #/np.exp(2*np.abs(kdiag)) 
    Hmat = np.diag(diagH)
    Hmat = Hmat +  K*cosmat
    
    return Hmat



def calc_h_from_n(Nfill, *args):
    
    cosmat, Nmat, U, K, NN, beta = args
    T = 1./beta
    #print('args = ', U, K, NN, 'Nfill = ', Nfill)
    ### calculate minimal n ###
    hmin = 1+2*U
    Hmat = build_H_N(cosmat, U, K, hmin, NN) #build_H_N(cosmat, U, K, h, NN) #

    eigval, evec = LA.eig(Hmat)
    eigvec = list(evec.T)
#     sort = np.argsort(w)
#     w = w[sort]
#     v = v[:,sort]
#     loop = False
                
    #N_min = (v[:,0].T).dot(Nmat).dot(v[:,0])
    N_min = expectation_value(Nmat, eigval, eigvec, T)
        
    ### calculate maximal n ###
    hmax = -1 -2*U
    Hmat = build_H_N(cosmat, U, K, hmax, NN)

    eigval, evec = LA.eig(Hmat)
    eigvec = list(evec.T)    
#     sort = np.argsort(w)
#     w = w[sort]
#     v = v[:,sort]
            
#     N_max = (v[:,0].T).dot(Nmat).dot(v[:,0])
    N_max = expectation_value(Nmat, eigval, eigvec, T)
    
    #print("N_min = ", N_min, " N_max = ", N_max)
    if (N_min-Nfill)*(N_max-Nfill)>0:
        print('ERROR - H OUTSIDE OF RANGE!!!')
    hnew = 0.
    herr = 1.
    while herr > 1e-4:
        Hmat = build_H_N(cosmat, U, K, hnew, NN)

        eigval, evec = LA.eig(Hmat)
        eigvec = list(evec.T)    
#     sort = np.argsort(w)
#     w = w[sort]
#     v = v[:,sort]
            
#     N_max = (v[:,0].T).dot(Nmat).dot(v[:,0])
        N_new = expectation_value(Nmat, eigval, eigvec, T)
            
        if (N_new-Nfill)>0:
            hmax = hnew
        if (N_new-Nfill)<0:
            hmin = hnew
            
        herr = np.abs(N_new-Nfill)
        hold = hnew
        hnew = (hmin+hmax)/2
        #print('herr = ', herr, 'hnew = ', hnew, 'N_new = ', N_new)
        
    cosPhi_exp = expectation_value(cosmat, eigval, eigvec, T)
    #cosPhi_exp = (v[:,0].T).dot(cosmat).dot(v[:,0])
    
    return hold, cosPhi_exp
 
def calc_h_from_n_Kselfcons(Nfill, *args):
    
    cosmat, Nmat, U, Ekin, NN, beta, cos_theta_old_cycl = args
    
    T = 1./beta
    
    hmin = 1+4*U
    maxerror = 1.
    while maxerror > 1e-4:
        K = 4.*Ekin*cos_theta_old_cycl
        Hmat = build_H_N(cosmat, U, K, hmin, NN) #build_H_N(cosmat, U, K, h, NN) #
        
        eigval, evec = LA.eig(Hmat)
        eigvec = list(evec.T)
        cosPhi_exp = expectation_value(cosmat, eigval, eigvec, T)
        maxerror = np.abs(cos_theta_old_cycl-cosPhi_exp)
        cos_theta_old_cycl = cosPhi_exp

    N_min = expectation_value(Nmat, eigval, eigvec, T)
        
    ### calculate maximal n ###
    hmax = -1 -4*U
    maxerror = 1.
    while maxerror > 1e-4:
        K = 4.*Ekin*cos_theta_old_cycl
        Hmat = build_H_N(cosmat, U, K, hmax, NN) #build_H_N(cosmat, U, K, h, NN) #
        
        eigval, evec = LA.eig(Hmat)
        eigvec = list(evec.T)
        cosPhi_exp = expectation_value(cosmat, eigval, eigvec, T)
        maxerror = np.abs(cos_theta_old_cycl-cosPhi_exp)
        cos_theta_old_cycl = cosPhi_exp
        
    N_max = expectation_value(Nmat, eigval, eigvec, T)
    
    #print("N_min = ", N_min, " N_max = ", N_max)
    if (N_min-Nfill)*(N_max-Nfill)>0:
        print('ERROR - H OUTSIDE OF RANGE!!!')
    hnew = 0.
    herr = 1.
    while herr > 1e-4:
        maxerror = 1.
        while maxerror > 1e-4:
            K = 4.*Ekin*cos_theta_old_cycl
            Hmat = build_H_N(cosmat, U, K, hnew, NN) #build_H_N(cosmat, U, K, h, NN) #

            eigval, evec = LA.eig(Hmat)
            eigvec = list(evec.T)
            cosPhi_exp = expectation_value(cosmat, eigval, eigvec, T)
            maxerror = np.abs(cos_theta_old_cycl-cosPhi_exp)
            cos_theta_old_cycl = cosPhi_exp

        N_new = expectation_value(Nmat, eigval, eigvec, T)
            
        if (N_new-Nfill)>0:
            hmax = hnew
        if (N_new-Nfill)<0:
            hmin = hnew
            
        herr = np.abs(N_new-Nfill)
        hold = hnew
        hnew = (hmin+hmax)/2
        
    cosPhi_exp = expectation_value(cosmat, eigval, eigvec, T)
    #cosPhi_exp = (v[:,0].T).dot(cosmat).dot(v[:,0])
    
    return hold, cosPhi_exp
