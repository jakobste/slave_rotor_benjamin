import numpy as np
from numpy import linalg as LA
from Rotor_MeanField import *

# Ix_mat = np.zeros((125,125))
# Ix_LT = np.zeros((25,25))

class Number_Slave:
    def __init__(self, Norb, Nfill, beta, IMPROVED_EXPR):
        self.Norb = Norb
        self.Nfill = Nfill
        self.beta = beta
        self.parameters_save = None
        self.IZ_L = np.diag(-np.arange(-Norb, Norb+1))
        
        Ix_L0 = np.zeros((int(Norb)*2+1, int(Norb)*2+1))
        # First guess for Ix_L -> has to be fixed if N ~= Norb !!!
        Ix_L0[int(Norb)*2,0] = 1.
        for n in range(int(Norb)*2):
            Ix_L0[n, n+1] = 1.
        
        if (Nfill != Norb) & IMPROVED_EXPR:
            K = -2*Norb*0.25
            args0 = 0., K, Norb, Nfill, beta
            newl, cosIxL_exp = calc_l_numberSlave(Ix_L0, self.IZ_L, *args0)
            old_cosIxL_exp = cosIxL_exp
            old_phi = 1
            new_phi = 1.01
            while (np.abs(cosIxL_exp-1.)>1e-6):
                Ix_L = np.copy(Ix_L0)
                Ix_L[2*int(Norb),0] = new_phi
                newl, cosIxL_exp = calc_l_numberSlave(Ix_L, self.IZ_L, *args0)
                dexpdphi = (cosIxL_exp-old_cosIxL_exp)/(new_phi-old_phi)
                dphi = (1.-cosIxL_exp)/dexpdphi
                old_cosIxL_exp = cosIxL_exp
                old_phi = new_phi
                new_phi = old_phi+dphi
                #print('cosIxL_exp =' + str(cosIxL_exp)+ ' old_phi = '+ str(old_phi) + ' new_phi = ' + str(new_phi))
            print('phi = '+str(new_phi) + ' IxL_exp = ' + str(cosIxL_exp))

            Ix_L[2*int(Norb),0] = new_phi
            self.Ix_L = np.copy(Ix_L)
        else:
            self.Ix_L = Ix_L0 
            #print(self.Ix_L)
        
    def cos_theta_exp_K(self, U, Elatt):
        K = 2*self.Norb*Elatt
        args = U, K, self.Norb, self.Nfill, self.beta
        newl, IxL_exp = calc_l_numberSlave(self.Ix_L, self.IZ_L, *args)
        self.parameters_save = U, Elatt, newl, 1., K
        return newl, IxL_exp
    
    # Result of self-consistency very sensitive to initial IxL_exp_0 !!!
    def cos_theta_selfcons(self, U, Elatt, IxL_exp_0):
        K0 = 2*self.Norb*Elatt*IxL_exp_0
        args0 = U, K0, self.Norb, self.Nfill, self.beta
        newl, IxL_exp = calc_l_numberSlave(self.Ix_L, self.IZ_L, *args0)
        #print('U = '+str(U))
        K = K0+0.
        while np.abs(IxL_exp-IxL_exp_0)>1e-4:
            K = 2*self.Norb*Elatt*IxL_exp
            IxL_exp_0 = IxL_exp
            args = U, K, self.Norb, self.Nfill, self.beta
            newl, IxL_exp = calc_l_numberSlave(self.Ix_L, self.IZ_L, *args)
        self.parameters_save = U, Elatt, newl, IxL_exp, K
        return newl, IxL_exp
    
    def GX_lehmann(self, Ntau):
        U, Elatt, l, IxL_exp, K = self.parameters_save 
        Hnew = K*(self.Ix_L + (self.Ix_L).T)/2. + ((self.IZ_L)**2)*U/2. + l*self.IZ_L          
        eigval, evec = LA.eig(Hnew)
        eigvec = list(evec.T)
        G_Lehmann = Lehmann_greensfun(self.Ix_L, eigval, eigvec, self.beta)
        
        tauvec = np.arange(float(Ntau))*self.beta/Ntau
        Grot = np.zeros(Ntau)
        for n, tau in enumerate(tauvec):
            Grot[n] = G_Lehmann.Greensfunction(tau)
        
        return Grot
    
    
        
# Ix_L = np.array([[0,1,0,0,0],[0,0,1,0,0],[0,0,0,1,0],[0,0,0,0,1],[1,0,0,0,0]])
# IZ_L = np.diag([2.,1.,0.,-1.,-2.])

# Ix_T = np.copy(Ix_L)
# Ix_S = np.copy(Ix_L)

# for ti in np.arange(5):
#     for tj in np.arange(5):
#         Ix_LT[ti*5:(ti+1)*5,tj*5:(tj+1)*5] = Ix_L*Ix_T[ti,tj]

# for si in np.arange(5):
#     for sj in np.arange(5):
#         Ix_mat[si*25:(si+1)*25,sj*25:(sj+1)*25] = Ix_LT*Ix_S[si,sj]


# IZ_L = np.zeros((125,125))
# IZ_T = np.zeros((125,125))
# IZ_S = np.zeros((125,125))
# for l in np.arange(125):
#     lmod = (l%5)
#     tmod = ((l - lmod)/5)%5
#     smod = ((l - lmod - 5*tmod)/25)%5
#     IZ_L[l,l] = 2 - lmod
#     IZ_T[l,l] = 2 - tmod
#     IZ_S[l,l] = 2 - smod

def expectation_value(operat, eigval, eigvec, T):
    exp_nonorm = T*0.
    norm = 0.
    opdim = operat.shape[0]
    mineig = np.amin(eigval)
    for n in np.arange(eigval.size):
        weight = np.exp(-(eigval[n]-mineig)/T)
        matel = (np.conjugate(eigvec[n]).reshape(1,opdim)).dot(operat).dot(eigvec[n].reshape(opdim,1))
        exp_nonorm = exp_nonorm + weight*matel
        norm = norm + weight
    return np.asscalar(exp_nonorm/norm)
    
def calc_l_numberSlave(Ix_L, IZ_L, *args): 
    U, K, Norb , Nfill, beta = args
    T = 1./beta
    # Ix_Lmin = np.copy(Ix_L)
    # Ix_Lmin[4,0] = 1./Ix_Lmin[4,0]
    cos_theta = (Ix_L + Ix_L.T)/2.
    #print('cos_theta = ', cos_theta)
    
    if (Nfill-Norb) != 0:
        lmax = 1.+2.*U
        Hnew = K*cos_theta + (IZ_L**2)*U/2. + lmax*IZ_L 
        
        
        eigval, evec = LA.eig(Hnew)
        eigvec = list(evec.T)

        maxL = expectation_value(IZ_L, eigval, eigvec, T) - (Nfill-Norb)

        lmin = -1.-2.*U
        Hnew =  K*cos_theta + (IZ_L**2)*U/2. + lmin*IZ_L 
        eigval, evec = LA.eig(Hnew)
        eigvec = list(evec.T)

        minL = expectation_value(IZ_L, eigval, eigvec, T) - (Nfill-Norb)

        if minL*maxL>0:
            print('Error in evaluation of l!!!'+ str(minL) + ' '+ str(maxL))

        newl = lmax - maxL*(lmin-lmax)/(minL-maxL)
        error = 1.
        while error > 1e-6:    
            # Hnew = Ham + newl*IZ_L
            Hnew =  K*cos_theta + (IZ_L**2)*U/2. + newl*IZ_L 
            eigval, evec = LA.eig(Hnew)
            eigvec = list(evec.T)

            newL = expectation_value(IZ_L, eigval, eigvec, T) - (Nfill-Norb)

            if newL>0:
                maxL = newL
                lmax = newl
            else:
                minL = newL
                lmin = newl

            oldl = newl
            newl = lmax - maxL*(lmin-lmax)/(minL-maxL)
            error = np.abs(oldl-newl)
    else:
        #print('L = 0')
        Hnew = K*cos_theta + (IZ_L**2)*U/2.
        eigval, evec = LA.eig(Hnew)
        eigvec = list(evec.T)
        newl = 0.

    IxL_exp = expectation_value(cos_theta, eigval, eigvec, T)
    return newl, np.abs(IxL_exp)