from Rotor_MeanField import *
from SlaveSpin_MeanField import *

def Slave_particle_SelfConsistency(params, METHOD_NAME):
    Ekin, Nfill_end, N_orbs, beta, Ntau, Udp, cos_theta_old_cycl = params

    SLAVE_SPIN = False
    if "spin" in METHOD_NAME.lower():
        SLAVE_SPIN = True

    NN = 12
    K = 4.*Ekin*cos_theta_old_cycl
    cosmat, Nmat = build_cosmat_Nmat(NN)
    
    if SLAVE_SPIN:
        SS1 = Number_Slave(N_orbs, Nfill_end, beta, False)
        l, cos_theta_exp = SS1.cos_theta_selfcons(Udp, Ekin, cos_theta_old_cycl)
        
        Grot  = SS1.GX_lehmann(Ntau)
        
    # ELSE SLAVE ROTOR #
    else:
        args = cosmat, Nmat, Udp, K, NN, beta
        h, cos_theta_exp = calc_h_from_n(Nfill_end - N_orbs, *args)

        
        ################### START WITH LOOPS ############################
        cos_thet_diff = np.abs(cos_theta_exp-cos_theta_old_cycl)
        cos_theta_old_cycl = cos_theta_exp
        while cos_thet_diff> 1e-4:
            K = 4.*Ekin*cos_theta_old_cycl
            args = cosmat, Nmat, Udp, K, NN, beta
            h, cos_theta_exp = calc_h_from_n(Nfill_end - N_orbs, *args)
            cos_thet_diff = np.abs(cos_theta_exp-cos_theta_old_cycl)
            cos_theta_old_cycl = cos_theta_exp

        cos_theta_err = np.abs(cos_theta_old_cycl - cos_theta_exp)
        cos_theta_old = cos_theta_exp

        tauvec = np.linspace(0, beta, Ntau)
        Grot   = np.zeros(Ntau)

        # creation operator rotor
        rotcreate = np.zeros((2*NN+1, 2*NN+1))
        rotcreate[2*NN, 0] = 1.
        for n in range(2*NN):
            rotcreate[n,n+1] = 1.
            
        #mpi.report('Slave Rotor cos_theta_err = '+str(cos_theta_err) + 'h = '+ str(h))
        Hmat = build_H_N(cosmat, Udp, K, h, NN) 
        eigval, evec = LA.eig(Hmat)
        eigvec = list(evec.T)

        G_Lehmann = Lehmann_greensfun(rotcreate.T, eigval, eigvec, beta) # Conjugated rotcreate!!!
        for n, tau in enumerate(tauvec):
            Grot[n] = G_Lehmann.Greensfunction(tau)

        del G_Lehmann
        
    
    return Grot, cos_theta_exp



def Slave_particle_Impurity(params, METHOD_NAME, self_cons_costheta=False, cos_theta_old=1.):
    Ekin, Nfill_end, N_orbs, beta, Ntau, Udp = params

    SLAVE_SPIN = False
    if "spin" in METHOD_NAME.lower():
        SLAVE_SPIN = True

    NN = 12
    K = 4.*Ekin*cos_theta_old
    cosmat, Nmat = build_cosmat_Nmat(NN)
    
    ################### CALCULATE LAGRANGE MULTIPLIER ############################
    if SLAVE_SPIN:
        SS1 = Number_Slave(N_orbs, Nfill_end, beta, False)
        l, cos_theta_exp = SS1.cos_theta_exp_K(Udp, Ekin) 
        
        Grot  = SS1.GX_lehmann(Ntau)
        
    else:
        args = cosmat, Nmat, Udp, K, NN, beta
        h, cos_theta_exp = calc_h_from_n(Nfill_end - N_orbs, *args)
        #print('Nfill_end = ' + str(Nfill_end) + ' N_orbs = ' + str(N_orbs) + ' Udp = ' + str(Udp) + ' Ekin = ' + str(Ekin) + ' NN = ' + str(NN) + ' beta = ' + str(beta) + 'cos_theta_old = ' + str(cos_theta_old))
        if self_cons_costheta:
            print('Self consistent Slave rotor impurity method!')
            while np.abs(cos_theta_exp -cos_theta_old) > 1e-4:
                cos_theta_old = cos_theta_exp
                K = 4.*Ekin*cos_theta_old
                args = cosmat, Nmat, Udp, K, NN, beta
                h, cos_theta_exp = calc_h_from_n(Nfill_end - N_orbs, *args)        
                #print('K = ' + str(K) + ' cos_theta_exp = ' + str(cos_theta_exp) + ' cos_theta_old = ' + str(cos_theta_old))
        ################### CALCULATE GX GREENS FUNCTION  ############################
        tauvec = np.linspace(0, beta, Ntau)
        Grot   = np.zeros(Ntau)

        # creation operator rotor
        rotcreate = np.zeros((2*NN+1, 2*NN+1))
        rotcreate[2*NN, 0] = 1.
        for n in range(2*NN):
            rotcreate[n,n+1] = 1.
            
        #mpi.report('Slave Rotor cos_theta_err = '+str(cos_theta_err) + 'h = '+ str(h))
        Hmat = build_H_N(cosmat, Udp, K, h, NN) 
        eigval, evec = LA.eig(Hmat)
        eigvec = list(evec.T)

        G_Lehmann = Lehmann_greensfun(rotcreate.T, eigval, eigvec, beta) # Conjugated rotcreate!!!
        for n, tau in enumerate(tauvec):
            Grot[n] = G_Lehmann.Greensfunction(tau)

        del G_Lehmann
    return Grot, cos_theta_exp
