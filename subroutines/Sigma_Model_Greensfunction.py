import numpy as np
from numpy import linalg as LA

class GreensFunction_boson:
    def __init__(self, params):
        
        self.is_freq = True # Per default, work in frequency space!
        if len(params) == 3:
            bet, n_point, freq_time = params
            if "time" in freq_time.lower():
                self.is_freq = False
            self.GX_inu = None
            self.inu_tail = None
                
        elif len(params) == 6:
            print (params)
            bet, n_point, Udp, h0, l0, DeltaX = params
            if not np.isscalar(DeltaX):
                assert DeltaX.size == (n_point*2-1)     
        else:
            print "Error!!! Number of input does not fit implementation!!!"
            
        self.beta = bet
        self.n_points = n_point
            
        self.inu_vec = 1j*(np.arange(float(n_point)*2.-1)-n_point+1)*2.*np.pi/bet
        self.tau_vec = np.arange(float(n_point)*2.+1)*bet/(n_point*2+1)
        self.dtau = self.tau_vec[1]-self.tau_vec[0]
        
        self.GX_tau = None
        self.GX_0 = None
        
        self.is_fourier_up_to_date = False
        
        # Construct Greens function from input parameters
        if len(params) == 6:
            self.GX_inu = (-self.inu_vec**2/(2.*Udp) - self.inu_vec*h0/Udp + l0 + DeltaX)**(-1)
            self.inu_tail = 1./(2.*Udp)
            self.DeltaX = DeltaX
            # Lagrange multipliers
            self.h_lagr = h0
            self.l_lagr = l0
            self.Udp = Udp           
            
    def enforce_constraints_jacobi(self, Nfill, Norb):
        GX_tau0 = self.GX_tau0()
        dGXdtau = self.dGX_dtau()
        start_vector = np.array([GX_tau0, dGXdtau - self.h_lagr])
        target_vector = np.array([1., (Nfill-Norb)*self.Udp])
        
        dpar = np.array([1.,1.])
        max_err = 1.
        while max_err > 1e-4:
            #calculate l derivative
            dx = 0.01*np.max(np.abs(dpar))
            self.update_GX_from_parameters(delta_l = dx)

            GX_tau0_dl = self.GX_tau0()
            dGXdtau_dl = self.dGX_dtau()

            #calculate h derivative
            self.update_GX_from_parameters(delta_h = dx)

            GX_tau0_dh = self.GX_tau0()
            dGXdtau_dh = self.dGX_dtau()

            # Define jacobi matrix [[df1/dl, df1/dh], [df2/dl, df2/dh]]
            jacobi_matrix = np.array([[GX_tau0_dl-GX_tau0, GX_tau0_dh-GX_tau0], [dGXdtau_dl-dGXdtau, dGXdtau_dh-dGXdtau]])/dx
            jacobi_matrix[1,1] = jacobi_matrix[1,1]-1. # because f2 = dGXdtau - h

            inverse_jacobi = LA.inv(jacobi_matrix)

            dpar = (target_vector-start_vector).dot(inverse_jacobi)

            self.l_lagr = self.l_lagr + dpar[0]
            self.h_lagr = self.h_lagr + dpar[1]

            self.update_GX_from_parameters()
            GX_tau0 = self.GX_tau0()
            dGXdtau = self.dGX_dtau()
            start_vector = np.array([GX_tau0, dGXdtau - self.h_lagr])
            #print("GX_tau0 = "+ str(GX_tau0)+ " dGXdtau - self.h_lagr =" + str(dGXdtau - self.h_lagr) + " Udp*(N-N/2) = " +str((Nfill-0.5)*self.Udp))
            
            max_err = np.max(np.abs(target_vector-start_vector))  
    
    # Enforce constraints, using the dichotomy procedure to enforce GX(tau=0) = 1  
    def enforce_constraint_dichotomy(self, Nfill, Norb):
        # starting with h0, enforce GX(tau=0) = 1
        self.enfore_GX0_dichotomy()
        
#         GX_tau0 = self.GX_tau0() # just check if GX(tau=0) = 1
#         print GX_tau0            # just check if GX(tau=0) = 1
        dGXdtau = self.dGX_dtau()
        
        delta_h = 0.01
        
        max_error = 1.
        while max_error > 1e-5:
            self.h_lagr = self.h_lagr + delta_h # maximal l_lagr
            self.update_GX_from_parameters()
            self.enfore_GX0_dichotomy()
            dGXdtau_dh = self.dGX_dtau()

            #print "(dGXdtau_dh + self.h_lagr) = " + str(dGXdtau_dh + self.h_lagr)
            
            diff_left = (self.Udp*(Nfill-Norb) - (dGXdtau_dh + self.h_lagr))
            delta_h = diff_left/(1. + (dGXdtau_dh-dGXdtau)/delta_h)
            dGXdtau = dGXdtau_dh
            max_error = np.abs(diff_left)
        
        return self.l_lagr, self.h_lagr

    def enfore_GX0_dichotomy(self):
        #just changed sign of Deltas
        if np.isscalar(self.DeltaX):
	    l_min = 1e-4 - (self.DeltaX).real
	else:
            l_min = 1e-4 - (self.DeltaX[self.n_points-1]).real
 
        self.l_lagr = l_min # minimal l_lagr
        self.update_GX_from_parameters()
        GX_tau0_min = self.GX_tau0()
        print('GX_tau0_min = '  +str(GX_tau0_min))
        
        l_max = self.Udp*8.
        self.l_lagr = l_max # maximal l_lagr
        self.update_GX_from_parameters()
        GX_tau0_max = self.GX_tau0()
        print('GX_tau0_max = '  +str(GX_tau0_max))        
        if (GX_tau0_max-1)*(GX_tau0_min-1) > 0:
            print "Dichotomy error !!!"
            print "GX_tau0_max " + str(GX_tau0_max) + " GX_tau0_min "+ str(GX_tau0_min)
            return 
        
        else:
            max_error = 1.
            while max_error > 1e-5:
                l_new = (l_max+l_min)/2.
                self.l_lagr = l_new # maximal l_lagr
                self.update_GX_from_parameters()
                GX_tau0 = self.GX_tau0()
                
                if GX_tau0 < 1:
                    l_max = l_new
                else:
                    l_min = l_new
                
                max_error = np.abs(GX_tau0-1.)
                #print "GX_tau0 " + str(GX_tau0) + " l_new "+str(l_new)
            
            return l_new           
            
        
    def update_GX_from_parameters(self, delta_l = 0, delta_h = 0):
        #just changed sign of Delta!
        self.GX_inu = ((self.inu_vec).imag**2/(2*self.Udp) - self.inu_vec*(self.h_lagr+delta_h)/self.Udp + (self.l_lagr+delta_l) + self.DeltaX)**(-1)
    
    def inu(self):
        return self.inu_vec
    
    def tau(self):
        return self.tau_vec
    
    def set_GX(self, GX):
        if self.is_freq:
            assert (GX.size == (self.n_points*2-1))&(self.is_freq)
            self.GX_inu = GX
        else:
            assert (GX.size == (self.n_points*2+1))&(not self.is_freq)
            self.GX_tau = GX
            self.GX_0 = GX[self.n_points]
    
            
    # Set high frequency tail 
    def set_tail(self, tail):
        self.inu_tail = tail
    
    def GX_data(self):
        if self.is_freq:
            return self.GX_inu
        else:
            return self.GX_tau
        
    def Fourier(self):
        if self.is_freq:
            print "Cannot perform fourier transform - already in frequency space!"
        else:
            nnu, ttau = np.meshgrid(self.inu_vec, self.tau_vec)
            self.GX_inu = (self.GX_tau).dot(np.exp(nnu*ttau))*self.dtau
            self.is_freq = True
    
    def Inverse_Fourier(self):
        if self.is_freq:
            ttau, nnu = np.meshgrid( self.tau_vec, self.inu_vec)
            if self.inu_tail == None:  
                print('Inv FFT No tail')             
                self.GX_tau = ((self.GX_inu).dot(np.exp(-nnu*ttau))).real/self.beta
            else:
                print('Inv FFT with tail')
                inu_nonzero = np.copy(self.inu_vec**(-2))
                inu_nonzero[self.n_points-1] = 0
                Gtau_finite = (self.GX_inu + self.inu_tail*(inu_nonzero)).dot(np.exp(-nnu*ttau))/self.beta
                Gtau_inf = self.beta*(1./6. - np.abs(self.tau_vec)/self.beta + (self.tau_vec/self.beta)**2)/2.
                self.GX_tau = Gtau_finite.real + self.inu_tail*Gtau_inf
            self.is_freq = False
            self.GX_0 = self.GX_tau[0]
        else:
            print "Cannot perform inverse fourier transform - already in tau space!"
            
        
    def GX_tau0(self):
        if self.is_freq:
            if self.inu_tail == None:               
                self.GX_0 = np.sum(self.GX_inu)/self.beta
            else:
                inu_nonzero = np.copy(self.inu_vec**(-2))
                inu_nonzero[self.n_points-1] = 0
                self.GX_0 = np.sum(self.GX_inu + self.inu_tail*(inu_nonzero))/self.beta + self.inu_tail*self.beta/12.
        else:
            self.GX_0 = self.GX_tau[0]
        
        return (self.GX_0).real
    
    # Derivative, defined as 1/2(dGX/dtau(tau=0-) + dGX/dtau(tau=0+))
    def dGX_dtau(self):
        if self.is_freq:          
            dGXdtau = -np.sum(self.inu_vec*self.GX_inu)/self.beta
        else:
            dGXdtau = ((self.GX_tau[1]-self.GX_tau[0]) + (self.GX_tau[0]-self.GX_tau[-1]))/(2.*self.dtau)
        
        return dGXdtau.real
    
        

