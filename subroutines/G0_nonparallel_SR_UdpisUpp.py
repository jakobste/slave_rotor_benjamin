from pytriqs.gf import *
from pytriqs.archive import HDFArchive
import numpy as np
from scipy import optimize
import sys
sys.path.append("/home/jakobste/DMFT/TRIQS/TEST_3DSQUARE_LATTICE/slave_rotor_benjamin/subroutines/") # REPLACE WITH NEW PATH!!!

from Gloc_parallel import *
#from Gloc_NNhopping_parallel import G0_square_lattice_NN
from Rotate_Basis import transf_Delta_phi, transf_Delta_auto
from Rotor_Greensfun import Rotor_Greens, Rotor_Greens_fixed_N
from Slave_MF_SelfConsistency import *


def calculate_G0_SR_UdpUpp(params, METHOD_NAME, mpirank):
    beta, Nomeg, Ntau, mu, Nfill_imp, Nfill_end, Udp, atom_en, t_hop, Sigma_iw, Gtilde_iw, filename, cos_exp_old, Ekin, GX_tau_old, sol, ALLOW_SR_UPDATE = params
    ################## Unparallelized routine to calculate G0 on different cores ####################
     
    # Create object for free Green's function on 2d square lattice
    G0_2dlatt = G0_square_lattice(100, atom_en, t_hop)


    sigma_iw_dat = np.squeeze(Sigma_iw.data) 
    if mpirank == 0:
        print('Calculate Gloc now')
    Gloc_new, Ndp = calc_G0(beta, Nomeg, sigma_iw_dat, mu, G0_2dlatt)
    # Delete object to free memory
    del G0_2dlatt 
    
    if mpirank == 0:
        print('Bring it to TRIQS gf format')
    # Set the hybridization function and G0_iw for the square lattice
    Gloc_initial = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)

    Gloc_initial.data[:,:,:] = Gloc_new #g0_unnorm  

    #Save unfit G0 function
    if mpirank == 0:
        with HDFArchive(filename,'a') as Results:
            Results['Gloc_iw'] = Gloc_initial

    fixed_coeff = np.array([[[0.,0.],[0., 0.]], [[1.,0.],[0., 1.]]], dtype=complex)
    om0 = 10.
    om1 = 30.
    t, e = Gloc_initial.fit_tail_on_window(int((om0*beta/np.pi-1)/2.), int((om1*beta/np.pi-1)/2.), fixed_coeff, 4, 4)
    Gloc_initial.replace_by_tail(t, int((50.*beta/np.pi-1)/2.)) #int(Nomeg*0.5))

    ############################################################################################################
    ############################################################################################################


    #### Calculate hybridization function from G0 ####
    Delta_init = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)
    Delta_init << iOmega_n + mu - atom_en - inverse(Gloc_initial) - Sigma_iw

    #Save unfit Delta_init function
    if mpirank == 0:
        with HDFArchive(filename,'a') as Results:
            Results['Delta_iw'] = Delta_init
    
    fixed_coeff = np.array([[[0.,0.],[0., 0.]]], dtype=complex)
    t, e = Delta_init.fit_tail_on_window(int((om0*beta/np.pi-1)/2.), int((om1*beta/np.pi-1)/2.), fixed_coeff, 4, 4)
    Delta_init.replace_by_tail(t, int((50.*beta/np.pi-1)/2.))

    
    GX_tau = None
    
    G0_initial = GfImFreq(indices=['d','p'], beta = beta, n_points = Nomeg)
    G0_initial << inverse(iOmega_n + mu - atom_en - Delta_init)
    
    ################# Introduce zeroth order slave rotor for Udp ##########################

    Delta_tau = GfImTime(indices=['d','p'], beta=beta, n_points = Ntau)
    Delta_tau << InverseFourier(Delta_init) #, fixed_coeff)

    Delta_SR_tau = GfImTime(indices=['d','p'], beta=beta, n_points = Ntau)
    Delta_SR = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)

    
#     GX0_iw = np.zeros(Nomeg)
#     h = 0.
#     params = [beta, Udp, h, Ntau]  
#     GX0_tau, dGX0dtau, tauvec, h_old = Rotor_Greens_fixed_N(params, Nfill_end)

    N_orbs = 2.
    maxerr = np.abs(Nfill_imp-Nfill_end)
    SR_UPDATE = False
    
    First_cycle = False
    if (Ekin == None):
        First_cycle = True
        if mpirank==0: print('FIRST CYCLE - no Ekin yet!!!')
    ##################### Calculate Kinetic energy of pseudo-fermions on lattice ############
    if First_cycle:
        Ekin = calc_Eimp_DMFT(beta, Nomeg, Delta_init, Gloc_initial, 1e-10)
        findEkin = [x for x in sys.argv if 'x_is_' in x]
        if len(findEkin) > 0:
            findEkin = findEkin[0]
        if 'x_is_' in findEkin:
            Ekin = float(findEkin[5:])
            if mpirank==0: print('Ekin init = ' + str(Ekin))
        if 'x2' in sys.argv:
            if mpirank ==0: print('Multiply Ekin x 2 !!!')
            Ekin = 2.*Ekin
        if 'x0_15' in sys.argv:
            if mpirank ==0: print('Multiply first Ekin x 1.5 !!!')
            Ekin = 1.5*Ekin
        SR_UPDATE = True
        if mpirank == 0:
            print('New Ekin = '+ str(Ekin) + ' (Updated)')
    else:
        ## Only update Ekin, and therefore GX if filling is sufficiently close to Nfill_end
        if ALLOW_SR_UPDATE: #(maxerr < Nfill_end*max_relative_error/2.):       
            Ekin = calc_Eimp_DMFT(beta, Nomeg, Delta_init, Gtilde_iw, 1e-10)
            if 'x2' in sys.argv:
                if mpirank ==0: print('Multiply Ekin x 2 !!!')
                Ekin = 2.*Ekin
            SR_UPDATE = True
            if mpirank == 0:
                print('New Ekin = ' + str(Ekin) + ' (Updated)' )
        ## Else don't update it
        else:
            if mpirank == 0:
                print('New Ekin = ' + str(Ekin))
                
    
    ##################### Calculate GX for Slave-particles  #################
    
    # Slave particle method 
    if ("spin" in METHOD_NAME.lower()) | ("rotor" in METHOD_NAME.lower()) :
        params = Ekin, Nfill_end, N_orbs, beta, Ntau, Udp

        ##### Calculating the Slave Particle Greens function ####
        if ("const" in METHOD_NAME.lower()): # <- STANDARD METHOD USED IN PAPER !!!!
            if (mpirank == 0): print('Slave rotor constant') 
            GX_tau, cos_theta_exp = Slave_particle_Impurity(params, METHOD_NAME, self_cons_costheta=False, cos_theta_old=cos_exp_old)
        else:
            if mpirank == 0: print('Slave rotor self-consistenly evaluated with K = -<cos theta>4Ekin')
            GX_tau, cos_theta_exp = Slave_particle_Impurity(params, METHOD_NAME, self_cons_costheta=True, cos_theta_old=cos_exp_old)
        if ('flipGX' in sys.argv):
            if mpirank == 0: print('Flip GX in tau!')
            GX_tau = np.flip(GX_tau,0)

        if mpirank == 0: print('New cos_theta_exp = ' + str(cos_theta_exp))
            
    # Sigma Model from Slave Rotor  
    elif "sigma" in METHOD_NAME.lower():        
        if ALLOW_SR_UPDATE | First_cycle:  
            G_imp_tau = GfImTime(indices=['d','p'], beta=beta, n_points = Ntau)
            G_imp_tau << InverseFourier(Gtilde_iw)

            params = beta, Nomeg, Nfill_end, N_orbs, Udp, Delta_tau, G_imp_tau
            GX_nu, GX_tau, lout, hout = calc_GX_sigma_DMFT(params)    
            if mpirank == 0: print('lout = '+str(lout) + ' hout = '+ str(hout))
            # Not properly defined here
        else:
            GX_tau = GX_tau_old
            
        cos_theta_exp = 1. 
        
    # Nothing 
    else:
        GX_tau = np.ones(Ntau)
        cos_theta_exp = 1.



    if ("const" in METHOD_NAME.lower()):
        if mpirank == 0: print('Constant factor Delta')
        Delta_SR_tau.data[:,0,0] = Delta_tau.data[:,0,0]*cos_theta_exp**2
        Delta_SR_tau.data[:,0,1] = Delta_tau.data[:,0,1]*cos_theta_exp**2
        Delta_SR_tau.data[:,1,0] = Delta_tau.data[:,1,0]*cos_theta_exp**2
        Delta_SR_tau.data[:,1,1] = Delta_tau.data[:,1,1]*cos_theta_exp**2
        
        Delta_SR << Fourier(Delta_SR_tau)
    else:
        if mpirank == 0: print('Dynamic factor Delta')
        if (sol == None):
            Delta_SR_tau.data[:,0,0] = Delta_tau.data[:,0,0]*GX_tau
            Delta_SR_tau.data[:,0,1] = Delta_tau.data[:,0,1]*GX_tau
            Delta_SR_tau.data[:,1,0] = Delta_tau.data[:,1,0]*GX_tau
            Delta_SR_tau.data[:,1,1] = Delta_tau.data[:,1,1]*GX_tau
            
            Delta_SR << Fourier(Delta_SR_tau)
        # Else use improved slave rotor expression -> NOT RELEVANT HERE !!!
        else:
            tauvec= np.arange(float(Ntau))*beta/Ntau
            nuvec = (np.arange(float(Ntau))-Ntau/2)*2.*np.pi/beta
            Gnu = np.zeros(Ntau, dtype=complex)
            for n, nu in enumerate(nuvec):
                Gnu[n] = np.sum(np.exp(1j*tauvec*nu)*GX_tau)*(tauvec[1]-tauvec[0])

            #sol = optimize.root(mod_GXnu, [21, 1], [Gnu, np.squeeze(G_backtilde.data)[:,0,0], G0_shell_fold, c_parameter])
            if mpirank == 0 : print('Optimized parameters to modify GX = ' + str(sol.x[0]) + ' ' + str(sol.x[1]))

            Gnumod = (1. - sol.x[1]*np.exp(-np.abs(nuvec)/c_parameter))*Gnu
            Gnumod[int(Ntau/2)] = sol.x[0]

            for n in np.arange(2*Nomeg):
                Delta_SR.data[n,0,0] = Delta_init.data[:,0,0].dot(np.roll(Gnumod, Ntau/2 + n+1)[:-1])/beta
                Delta_SR.data[n,0,1] = Delta_init.data[:,0,1].dot(np.roll(Gnumod, Ntau/2 + n+1)[:-1])/beta
                Delta_SR.data[n,1,0] = Delta_init.data[:,1,0].dot(np.roll(Gnumod, Ntau/2 + n+1)[:-1])/beta
                Delta_SR.data[n,1,1] = Delta_init.data[:,1,1].dot(np.roll(Gnumod, Ntau/2 + n+1)[:-1])/beta
        
    #mpi.report('Delta_tau ', Delta_tau.data[500,:,:], ' Delta_SR_tau ', Delta_SR_tau.data[500,:,:], ' SR ', GX0_tau[500])

    #Delta_SR = GfImFreq(indices=['d','p'], beta=beta, n_points = Nomeg)
    
                    
    ################################## Rotate basis to minimize sign problem ##################################

    #Delta0 = Delta_init.data[Nmatsub/2 +1]

    # This tail fit should not be necessary!!!
    fixed_coeff = np.array([[[0.,0.],[0., 0.]]], dtype=complex)
    t, e = Delta_SR.fit_tail_on_window(int((om0*beta/np.pi-1)/2.), int((om1*beta/np.pi-1)/2.), fixed_coeff, 4, 4)
    Delta_SR.replace_by_tail(t, int((50.*beta/np.pi-1)/2.))
    #### Check if Delta_SR is real in tau ####
    if not Delta_SR.is_gf_real_in_tau():
        if mpirank == 0: print('Attention, Delta_SR is not real in tau!!! ...Make real in tau.')
        Delta_SR = Delta_SR.make_real_in_tau()
    

        
    G0_solver = GfImFreq(indices=['d','p'], beta = beta, n_points = Nomeg)
    G0_solver << inverse(iOmega_n + mu - atom_en - Delta_SR)
    
    G0_inverse = GfImFreq(indices=['d','p'], beta = beta, n_points = Nomeg)
    G0_inverse << iOmega_n + mu - atom_en - Delta_SR
    
    #### Integrate out the p-orbitals ######
    Gd_solver = GfImFreq(indices=['d'], beta = beta, n_points = Nomeg)
    Gd_solver['d','d'] << inverse(G0_inverse['d','d'] - inverse(G0_inverse['p','p'])*(atom_en[0,1] + Delta_SR['d','p'])*(atom_en[1,0] + Delta_SR['p', 'd']))
        
    #### Check if Gd_solver is real in tau ####
    if not Gd_solver.is_gf_real_in_tau():
        if mpirank == 0: print('Attention, Gd_solver is not real in tau!!! ...Make real in tau.')
    Gd_solver = Gd_solver.make_real_in_tau()
        

    return Gd_solver, G0_solver, Delta_SR, Delta_init, G0_initial, GX_tau, cos_theta_exp, Ekin, SR_UPDATE
